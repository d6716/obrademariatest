<?php 
include("admin/tupi.inicializar.php");
$obItemObra = new ItemObraDeMaria();
$paragrafos = $obItemObra->getRows(0,9999,array("ordem"=>"ASC"));

$menusite = 2; 
include('include-header.php');?>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>	
<?php include('include-menu.php'); ?>
<!-- blog breadcrumb version one strat here -->
<section class="breadcrumb-blog-version-one">
	<div class="single-bredcurms" style="background-image:url('images/bgimage/testimonial.png');">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="bredcrums-content">
						<h2>Sobre N�s</h2>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li class="active"><a href="aboutus.php">Sobre N�s</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!-- blog breadcrumb version one end here -->

<!-- google map start  -->
<section class="section-paddings">
	<div class="container">
<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="tour-description">
										<h4>Um Pouco da nossa hist�ria</h4>
										<div class="main-timeline">
											<?php foreach ($paragrafos as $key => $value) {
												
											?>
											<div class="timeline">
												<div class="timeline-content left">
												<div class="row">
													<div class="col-md-7">
													<span class="timeline-icon"><?=$value->ordem?></span>
													<div class="paragrafo"><?= $value->texto?>													
													</div>
													</div>
													<div class="col-md-5">
													<?php if($value->foto != null && $value->foto != ''){ ?>
													<img src="img/reviews/<?=$value->foto?>" width="auto" />
													<?php } ?>
													</div>
												</div>	
												
												</div>
											</div>
											<?php } ?>
											
										</div>
									</div>
								</div>
							</div>
							</div>
						</section>


<?php include('include-footer-area.php');?>

<div class="to-top pos-rtive">
	<a href="#"><i class = "fa fa-angle-up"></i></a>
</div> <!-- Scroll to top jump button end-->

 <?php include('include-footer.php');?>
</body>
</html>
