<?php
$reviewsReview  = new Review();
$reviews = $reviewsReview->getRandomicos(50);
if(count($reviews)>0){
?>
<!-- testimonial area start here -->

<section id="peregrinos" class="section-paddings testimonial-two testimonial-area image-bg-padding-100">
	<div class="testimonial-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center" >
						<h2 style="color: #ffffff;">Experi�ncias e Testemunhos de Nossos Peregrinos</h2>
						<p style="color: #ffffff;">Veja o que nossos peregrinos falam sobre n�s</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<!-- start top media -->
					<div class="top-testimonial-image row slick-pagination">
						<div class="carousel-images slider-nav-two col-sm-8 col-sm-offset-2">
						<?php foreach($reviews as $key => $rev){ ?>
							<div>
								<span><img src="img/reviews/<?=$rev->photo?>" alt="1" class="img-responsive img-circle"></span>
							</div>
                           
                            <? } ?>		
							
							
						</div>
					</div><!-- end top media images -->

					<!-- bottom testimonial message -->
					<div class="block-text row">
						<div class="carousel-text slider-for-two col-sm-8 col-sm-offset-2">

						<?php foreach($reviews as $key => $rev){ ?>
							<div class="single-box">
								<p style="color: #ffffff;"><?= $rev->review?></p>
								<div class="client-bio">
									<h3><?= $rev->name?></h3>
									<span style="color: #ffffff;"><?= $rev->local?></span><br/>
									<span style="color: #ffffff;"><?= substr($reviewsReview->convdata($rev->date,"mtnh"),0,10);?></span>
								</div>

							</div>
                            <? } ?>

							
							
						</div>
					</div><!-- bottom testimonial message -->
				</div><!-- /.block-text -->
			</div>
		</div>
	</div>
</section>
<?php } ?>