
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


DROP TABLE IF EXISTS `ag_roteiro`;
CREATE TABLE IF NOT EXISTS `ag_roteiro` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grupo` int(11) NOT NULL,
  `card_image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `card_title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `card_value` decimal(10,2) NOT NULL,
  `card_description` varchar(500) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_roteiro_grupo` (`grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

DROP TABLE IF EXISTS `ag_slide`;
CREATE TABLE IF NOT EXISTS `ag_slide` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roteiro` bigint(20) NOT NULL,
  `image` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `sub_title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `buttom_text` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_slide_roteiro` (`roteiro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

ALTER TABLE `ag_slide`
  ADD CONSTRAINT `fk_slide_roteiro` FOREIGN KEY (`roteiro`) REFERENCES `ag_roteiro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `ag_roteiro`
  ADD CONSTRAINT `fk_roteiro_grupo` FOREIGN KEY (`grupo`) REFERENCES `ag_grupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `ag_roteiro` ADD `continent` VARCHAR(20) NOT NULL AFTER `card_description`, ADD INDEX `idx_continente` (`continent`); 
ALTER TABLE `ag_roteiro` ADD `likes` INT NOT NULL AFTER `continent`; 
ALTER TABLE `ag_roteiro` CHANGE `likes` `likes` INT(11) NOT NULL DEFAULT '0'; 
ALTER TABLE `ag_roteiro` DROP `card_value`;
ALTER TABLE `ag_roteiro` ADD `unlikes` INT NOT NULL DEFAULT '0' AFTER `likes`; 
ALTER TABLE `ag_roteiro` ADD `image` VARCHAR(255) NULL AFTER `unlikes`, ADD `title` VARCHAR(255) NULL AFTER `image`, ADD `description` TEXT NULL AFTER `title`; 

DROP TABLE IF EXISTS `ag_etinerario`;
CREATE TABLE IF NOT EXISTS `ag_etinerario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `roteiro` bigint(20) NOT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_etinerario_roteiro` (`roteiro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ag_foto`
--

DROP TABLE IF EXISTS `ag_foto`;
CREATE TABLE IF NOT EXISTS `ag_foto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `roteiro` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_foto_roteiro` (`roteiro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


DROP TABLE IF EXISTS `ag_review`;
CREATE TABLE IF NOT EXISTS `ag_review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `review` varchar(1000) COLLATE latin1_general_ci NOT NULL,
  `date` datetime NOT NULL,
  `cliente` int(11) NOT NULL,
  `roteiro` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_review_roteiro` (`roteiro`),
  KEY `fk_review_cliente` (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


DROP TABLE IF EXISTS `ag_video`;
CREATE TABLE IF NOT EXISTS `ag_video` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `roteiro` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_video_roteiro` (`roteiro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


ALTER TABLE `ag_etinerario`
  ADD CONSTRAINT `fk_etinerario_roteiro` FOREIGN KEY (`roteiro`) REFERENCES `ag_roteiro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `ag_foto`
  ADD CONSTRAINT `fk_foto_roteiro` FOREIGN KEY (`roteiro`) REFERENCES `ag_roteiro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `ag_review`
  ADD CONSTRAINT `fk_review_cliente` FOREIGN KEY (`cliente`) REFERENCES `ag_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_review_roteiro` FOREIGN KEY (`roteiro`) REFERENCES `ag_roteiro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `ag_video`
  ADD CONSTRAINT `fk_video_roteiro` FOREIGN KEY (`roteiro`) REFERENCES `ag_roteiro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `ag_grupo` ADD `local` VARCHAR(150) NULL AFTER `desconto_avista`, ADD `idade_minima` INT NULL AFTER `local`, ADD `max_pessoa` INT NULL AFTER `idade_minima`, ADD `duracao` INT NULL AFTER `max_pessoa`;
ALTER TABLE `ag_review` ADD `name` VARCHAR(150) NOT NULL AFTER `roteiro`, ADD `email` VARCHAR(150) NOT NULL AFTER `name`; 
ALTER TABLE `ag_review` CHANGE `cliente` `cliente` INT(11) NULL; 
ALTER TABLE `ag_roteiro` ADD `countDown` TINYINT NOT NULL DEFAULT '0' AFTER `description`; 
ALTER TABLE `ag_roteiro` CHANGE `continent` `continent` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL; 
ALTER TABLE `ag_roteiro` ADD `publish` TINYINT(1) NOT NULL DEFAULT '0' AFTER `continent`; 


INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`) VALUES ('50', NULL, 'Site', NULL);
INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`) VALUES ('51', '50', 'Roteiros', 'roteiro.php'), ('52', '50', 'Sliders', 'slider.php');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '50', '1'), (NULL, '51', '1');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '52', '1');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '50', '15'), (NULL, '51', '15');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '52', '15');
ALTER TABLE `ag_slide` ADD `publish` TINYINT(1) NOT NULL DEFAULT '0' AFTER `buttom_text`; 


DROP TABLE IF EXISTS `ag_galeria`;
CREATE TABLE IF NOT EXISTS `ag_galeria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `grupo` int(11) NOT NULL,
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_grupo_galeria` (`grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
ALTER TABLE `ag_galeria` ADD CONSTRAINT `fk_grupo_galeria` FOREIGN KEY (`grupo`) REFERENCES `ag_grupo`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`) VALUES ('53', '50', 'Galerias', 'galeria.php'); 
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '53', '1'), (NULL, '53', '15'); 


DROP TABLE IF EXISTS `ag_galeria_foto`;
CREATE TABLE IF NOT EXISTS `ag_galeria_foto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `galeria` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galeria_foto` (`galeria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


ALTER TABLE `ag_galeria_foto`
  ADD CONSTRAINT `fk_galeria_foto` FOREIGN KEY (`galeria`) REFERENCES `ag_galeria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;




-- 12/08/2020
ALTER TABLE `ag_galeria_foto` ADD `type` INT NOT NULL DEFAULT '0' AFTER `galeria`; 
ALTER TABLE `ag_galeria_foto` ADD `description` VARCHAR(500) NULL AFTER `type`; 

ALTER TABLE `ag_review` DROP FOREIGN KEY fk_review_cliente;
ALTER TABLE `ag_review` DROP INDEX `fk_review_cliente`;
ALTER TABLE `ag_review` DROP `email`;
ALTER TABLE `ag_review` DROP `cliente`;
ALTER TABLE `ag_review` ADD `photo` VARCHAR(255) NOT NULL AFTER `name`; 

-- 13/08/2020
ALTER TABLE `ag_review` CHANGE `roteiro` `roteiro` BIGINT(20) NULL; 
ALTER TABLE `ag_review` DROP FOREIGN KEY `fk_review_roteiro`; ALTER TABLE `ag_review` ADD CONSTRAINT `fk_review_roteiro` FOREIGN KEY (`roteiro`) REFERENCES `ag_roteiro`(`id`) ON DELETE SET NULL ON UPDATE CASCADE; 
ALTER TABLE `ag_review` ADD `local` VARCHAR(255) NOT NULL AFTER `photo`; 
INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`) VALUES ('54', '50', 'Comentários', 'comentario.php');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '54', '1'), (NULL, '54', '15');

ALTER TABLE `ag_grupo` ADD `bit_boleto` TINYINT(1) NULL DEFAULT '0' AFTER `duracao`, ADD `bit_cartao` TINYINT(1) NULL DEFAULT '0' AFTER `bit_boleto`, ADD `bit_cheque` TINYINT(1) NULL DEFAULT '0' AFTER `bit_cartao`, ADD `bit_customizado` TINYINT(1) NULL DEFAULT '0' AFTER `bit_cheque`, ADD `parcela_boleto` TINYINT(2) NULL DEFAULT '12' AFTER `bit_customizado`, ADD `parcela_cartao` TINYINT(2) NULL DEFAULT '12' AFTER `parcela_boleto`, ADD `parcela_cheque` TINYINT(2) NULL DEFAULT '12' AFTER `parcela_cartao`, ADD `nome_customizado` VARCHAR(150) NULL AFTER `parcela_cheque`; 
ALTER TABLE `ag_grupo` ADD `text_customizado` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `nome_customizado`;
ALTER TABLE `ag_grupo` ADD `bit_transferencia` TINYINT NOT NULL DEFAULT '1' AFTER `text_customizado`;
ALTER TABLE `ag_grupo` ADD `cotacao_customizado` DECIMAL(10,2) NULL DEFAULT '1.00' AFTER `bit_transferencia`;
ALTER TABLE `ag_grupo` ADD `bit_adesao_customizado` TINYINT(1) NOT NULL DEFAULT '1' AFTER `cotacao_customizado`; 
-- ALTER TABLE `ag_grupo` CHANGE `text_customizado` `text_customizado` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;


ALTER TABLE `ag_roteiro` ADD `padre_name` VARCHAR(200) NULL AFTER `publish`, ADD `padre_image` VARCHAR(255) NULL AFTER `padre_name`; 
INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`) VALUES ('55', '1', 'Configurações', 'configuracoes.php');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '55', '1'), (NULL, '55', '15');
INSERT INTO `ag_agendamento` (`id`, `descricao`, `data`, `destinatarios`) VALUES ('7', 'UrlRevista', '2020-08-13', 'http://online.pubhtml5.com/vvvl/vhrc/');

ALTER TABLE `ag_galeria_foto` ADD `name_tumb` VARCHAR(255) NOT NULL AFTER `description`;
ALTER TABLE `ag_foto` ADD `name_thumb` VARCHAR(255) NOT NULL AFTER `roteiro`; 

alter table `ag_venda_site` add `percentual_entrada` decimal(10,2) DEFAULT '0.00' AFTER `desconto`,
  add `valor_entrada` decimal(10,2) DEFAULT '0.00' after `percentual_entrada`,
  add `valor_resto` decimal(10,2) DEFAULT '0.00' after `valor_entrada`; 
-- ALTER TABLE `ag_venda_site` CHANGE `percentual_entrada` `percentual_entrada` DECIMAL(10,2) NULL DEFAULT '0'; 

DROP TABLE IF EXISTS `ag_item_obra`;
CREATE TABLE IF NOT EXISTS `ag_item_obra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL,
  `texto` text COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`) VALUES ('56', '50', 'A Obra de Maria', 'aobrademaria.php');
INSERT INTO `ag_menuperfil` (`id`, `idMenu`, `idPerfil`) VALUES (NULL, '56', '1'), (NULL, '56', '15');
COMMIT;
