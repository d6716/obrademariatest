ALTER TABLE `ag_checkout_cielo` CHANGE `venda` `venda` BIGINT(20) NULL; 
ALTER TABLE `ag_checkout_cielo` ADD `participante` INT NULL AFTER `test_transaction`; 
ALTER TABLE `ag_checkout_cielo` ADD CONSTRAINT `fk_participante_cielo` FOREIGN KEY (`participante`) REFERENCES `ag_participante`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
ALTER TABLE `ag_gerencianet` CHANGE `id_venda_site` `id_venda_site` BIGINT(20) NULL; 
ALTER TABLE `ag_gerencianet` ADD `participante` INT NULL AFTER `id_venda_site`; 
ALTER TABLE `ag_gerencianet` ADD CONSTRAINT `fk_participante_gn` FOREIGN KEY (`participante`) REFERENCES `ag_participante`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 