ALTER TABLE `ag_pagamento` ADD `id_cielo` BIGINT NULL AFTER `pago`; 
ALTER TABLE `ag_pagamento` ADD CONSTRAINT `fk_cielo_pagamento` FOREIGN KEY (`id_cielo`) REFERENCES `ag_checkout_cielo`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
INSERT INTO `ag_bandeira` (`id`, `descricao`, `imagem`) VALUES (NULL, 'discover', 'discover.png'), (NULL, 'jcb', 'jcb.jpg'); 