-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de gera��o: 18-Set-2020 �s 11:23
-- Vers�o do servidor: 8.0.18
-- vers�o do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Banco de dados: `obrademariadf1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ag_lista_espera`
--

DROP TABLE IF EXISTS `ag_lista_espera`;
CREATE TABLE IF NOT EXISTS `ag_lista_espera` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `telefone` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `id_grupo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grupo_lista_espera` (`id_grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Restri��es para despejos de tabelas
--

--
-- Limitadores para a tabela `ag_lista_espera`
--
ALTER TABLE `ag_lista_espera`
  ADD CONSTRAINT `fk_grupo_lista_espera` FOREIGN KEY (`id_grupo`) REFERENCES `ag_grupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ag_roteiro` ADD `bit_lista_espera` TINYINT(1) NOT NULL DEFAULT '0' AFTER `pesquisa`; 

COMMIT;
