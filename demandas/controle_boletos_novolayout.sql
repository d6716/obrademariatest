-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de gera��o: 23-Set-2020 �s 19:32
-- Vers�o do servidor: 8.0.18
-- vers�o do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Banco de dados: `obrademariadf1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ag_controle_boleto`
--

DROP TABLE IF EXISTS `ag_controle_boleto`;
CREATE TABLE IF NOT EXISTS `ag_controle_boleto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_participante` int(11) NOT NULL,
  `dia_vencimento` int(11) NOT NULL,
  `numero_parcelas` int(11) NOT NULL,
  `data_criacao` datetime NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `valor_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_particpante_controle_boleto` (`id_participante`),
  KEY `fk_grupo_controle_boleto` (`id_grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ag_controle_boleto_parcela`
--

DROP TABLE IF EXISTS `ag_controle_boleto_parcela`;
CREATE TABLE IF NOT EXISTS `ag_controle_boleto_parcela` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_controle_boleto` bigint(20) NOT NULL,
  `valor_moeda` decimal(10,2) NOT NULL,
  `valor_real` decimal(10,2) DEFAULT NULL,
  `cotacao` decimal(10,2) DEFAULT NULL,
  `data_vencimento` date NOT NULL,
  `bit_gerado` tinyint(4) NOT NULL DEFAULT '0',
  `numero` int(11) NOT NULL,
  `gn_status` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `gn_charge_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `gn_mumero_febraban` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `gn_url_boleto` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_controle_parcela` (`id_controle_boleto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Restri��es para despejos de tabelas
--

--
-- Limitadores para a tabela `ag_controle_boleto`
--
ALTER TABLE `ag_controle_boleto`
  ADD CONSTRAINT `fk_grupo_controle_boleto` FOREIGN KEY (`id_grupo`) REFERENCES `ag_grupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_particpante_controle_boleto` FOREIGN KEY (`id_participante`) REFERENCES `ag_participante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `ag_controle_boleto_parcela`
--
ALTER TABLE `ag_controle_boleto_parcela`
  ADD CONSTRAINT `fk_controle_parcela` FOREIGN KEY (`id_controle_boleto`) REFERENCES `ag_controle_boleto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `ag_menu` ADD `icon` VARCHAR(100) NULL AFTER `url`; 
UPDATE `ag_menu` SET `icon` = 'fas fa-fw fa-table' WHERE `ag_menu`.`id` = 1; 
UPDATE `ag_menu` SET `icon` = 'fas fa-fw fa-route' WHERE `ag_menu`.`id` = 2; 
UPDATE `ag_menu` SET `icon` = 'fas fa-fw fa-money-check' WHERE `ag_menu`.`id` = 3; 
UPDATE `ag_menu` SET `icon` = 'fas fa-fw fa-print' WHERE `ag_menu`.`id` = 4; 
UPDATE `ag_menu` SET `icon` = 'fas fa-fw fa-passport' WHERE `ag_menu`.`id` = 50; 



INSERT INTO `ag_menu` (`id`, `idMenuPai`, `descricao`, `url`, `icon`) VALUES ('57', '2', 'Controle de Boletos', 'controleBoletosPainel.php', NULL); 

--25/09
ALTER TABLE `ag_controle_boleto_parcela` ADD `data_pagamento` DATE NULL AFTER `gn_url_boleto`; 
COMMIT;


