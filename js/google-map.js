

/*-------------------------------------------------------------
  07. Google Map js
---------------------------------------------------------------*/


                function initialize() {
                    var mapOptions = {
                        zoom: 16,
                        scrollwheel: true,
                        center: new google.maps.LatLng(-15.7989087,-47.8955424),
                    };
                    var map = new google.maps.Map(document.getElementById('googleMap'),
                        mapOptions
                    );
                    var marker = new google.maps.Marker({
                    position: map.getCenter(),
                    animation:google.maps.Animation.BOUNCE,
                    icon: 'img/iconmap.png',
                    url : 'https://maps.app.goo.gl/qGEP8aemcwq7YxQt7',
                    title : 'Agencia de Viagens Obra de Maria DF',
                    map: map
                    });


                    map.setOptions({});

    var styles = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#37b721"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
];

    map.setOptions({styles: styles});   
    google.maps.event.addListener(marker, 'click', function() {
        window.location.href = this.url;
    });
                }
                
                google.maps.event.addDomListener(window, 'load', initialize);
                

				
				
				
				
				
				
