
<header>
	<div class="header-top-area">
		<div class="container">
			<div class="row">
				<div class="header-top-left">
					<div class="col-md-10 col-sm-10 col-xs-12">
						<ul class="header-top-contact">
							<li><i class="fa fa-map-marker" aria-hidden="true"></i>SRTVS 701, Bloco II, Sala 208, Ed. Chateaubriand</li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>61 3201-5116 | <i class="fa fa-whatsapp" aria-hidden="true"></i>61 98352-0475</li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i>brasilia@obrademaria.com.br</li>
						</ul>
					</div>
				</div>
				<div class="header-top-right text-right">
					<div class="col-md-2 col-sm-2 col-xs-12 book-tab">
						<div class="book-btn">
							<a href="packages.php">Vendas Online</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- header top end -->

	<div class="header-bottom-area" id="stick-header">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12 tap-v-responsive">
					<div class="logo-area">
						<a href="index.php"><img src="img/obra-logo-branco.png" alt="">
						</a>
					</div>
				</div>
				<div class="col-md-10">
					<nav>
						<ul class="main-menu text-right">
							<li class="<?= $menusite == 0 ? 'active' : '';?>"><a href="index.php">Home</a>
								
							</li>
							<li class="<?= $menusite == 1 ? 'active' : '';?>"><a href="packages.php">Roteiros</a>
								
							</li>
							<li class="<?= $menusite == 2 ? 'active' : '';?>"><a href="aboutus.php">Hist�ria da Obra de Maria</a>
								
							</li>
							<?php if($menusite == 0) { ?>
							<li><a href="#" class="to-peregrinos">Nossos Peregrinos</a>								
							</li>
							<?php } ?>
							<li class="<?= $menusite == 4 ? 'active' : '';?>"><a target="_blank" href="revista.php">Revista</a>
							</li>
							<li class="<?= $menusite == 3 ? 'active' : '';?>"><a href="contact.php">Contato</a>
							</li>
						</ul>
					</nav>
				</div> <!-- main menu end here -->
			</div>
		</div>
	</div> <!-- header-bottom area end here -->
</header> <!-- header area end here -->