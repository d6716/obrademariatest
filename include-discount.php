<?php
$disRoteiro = new Roteiro();
$disReturn = $disRoteiro->getDescontosRandomicos(3);
if($disReturn){
?>
<!-- discount & deals section start here -->
<section class="section-paddings">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="section-title text-center">
					<h2>Oportunidades de descontos</h2>
					<p>Aproveite o melhor dos seus sonhos com pre�os inperd�veis</p>
				</div>
			</div>
		</div>
		<div class="row">
			<?php foreach ($disReturn as $key => $value) {
				# code...
				$local = explode(",",$value->grupo->local);
				$local = $local[0];
			?>
			<!-- single dicount -->
			<div class="col-sm-6 col-md-4 single-item">
				<div class="single-discount-deal">
					<figure>
						<img src="img/packages/<?=$value->cardImage?>" alt="">
						<?php if($value->grupo->descontoAVista > 0 ){?>
							<figcaption>
							<div class="offer-content">
								<div class="circle-offer">
									<p><?=$value->grupo->descontoAVista?>%
										<br>Off</p>
								</div>
								<div class="offer-details-2">
									<h3>Melhor Roteiro em <?=$local?></h3>
									<p><?= $value->grupo->duracao ?> dias <del><?= $value->grupo->moeda->cifrao." ".$disRoteiro->money($value->grupo->valorPacote,"atb") ?></del> <span><?= $value->grupo->moeda->cifrao." ".$disRoteiro->money($value->grupo->valorPacote - ($value->grupo->valorPacote*($value->grupo->descontoAVista/100)),"atb")  ?></span>
									</p>
								</div>
							</div>
							<div class="travel-book-btn">
								<a href="package.php?id=<?=$value->id?>" class="travel-booking-btn hvr-fade">Detalhes</a>
							</div>
						</figcaption>
						<?php }else{ ?>
							<figcaption>
							<div class="offer-content">
								
							<div class="circle-offer">
									<p>Super<br/>Oferta</p>
								</div>
								<div class="offer-details-2">
									<h5 class="color-two"><?=$value->cardTitle?></h5>
									<h3></h3>
									<p><?=$value->textoPromocao?></p>
								</div>
							</div>
							<div class="travel-book-btn">
								<a href="package.php?id=<?=$value->id?>" class="travel-booking-btn hvr-fade">Detalhes</a>
							</div>
						</figcaption>
						<?php }?>
					</figure>
				</div>
			</div><!--end single dicount -->
			<? }?>			
		</div>
	</div>
</section> <!-- discount & deals section end here here -->
<? }?>