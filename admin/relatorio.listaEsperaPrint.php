<?php
include("tupi.inicializar.php");
$codTemplate = "relatorioHorizontal";
include("tupi.template.inicializar.php"); 
$codAcesso = 13;
include("tupi.seguranca.php");
//configura o grupo na pagina
$oGrupo = new Grupo();
$idGrupo = $oGrupo->md5_decrypt($_REQUEST['idGrupo']);
$oGrupo->getById($idGrupo);

$tpl->COD_GRUPO = $oGrupo->id;
$tpl->NOME_GRUPO = $oGrupo->nomePacote;
$tpl->ID_GRUPO_HASH = $oGrupo->md5_encrypt($oGrupo->id);
$obj = new ListaEspera();
$strBusca = isset($_REQUEST['busca']) ? str_replace(".","",str_replace("-","",$_REQUEST['busca'])) : "";
$rs = $obj->pesquisa(0,99999,$idGrupo,$strBusca);
$i = 1;
foreach($rs as $key => $lista){
    //calcula o status dos pagamentos;
   $tpl->ID = $i;
   $tpl->NOME = $lista->nome;
   $tpl->EMAIL = $lista->email;
   $tpl->TELEFONE = $lista->telefone;
    $i++;
    $tpl->block('BLOCK_ITEM_LISTA');
}
if(!isset($_REQUEST['tupiSendEmail']))
$tpl->block("BLOCK_ENVIO_EMAIL");
include("tupi.template.finalizar.php"); 