<?php 
include("tupi.inicializar.php");
$newTemplate = 1; 
include("tupi.template.inicializar.php"); 
$codAcesso = 13;
include("tupi.seguranca.php");

$obj = new ControleBoletoParcela();
$ag = new Agendamento();
$og = new Grupo();
$ag->getById(6);
$cotacao = $ag->destinatarios;
$tpl->COTACAO = $obj->money($cotacao,"atb");
$totalDollar = $obj->getTotalValorFaturadoByMoeda(1)*$cotacao;
$totalReal = $obj->getTotalValorFaturadoByMoeda(2);
$totalRecebidoMes = $obj->getTotalValorRecebidoMes();
$totalVencidos = $obj->getTotalVencidos();
$totalBoletos = $obj->getTotalBoletos();
$inadinplencia = @ceil(($totalVencidos*100)/$totalBoletos);
$grupos = $og->getGruposAndamento();
$tpl->COTACAO_JAVAS = $cotacao;
$tpl->TOTAL_FATURAR = $obj->money($totalDollar+$totalReal,"atb");
$tpl->VALOR_RECEBIDO_MES = $obj->money($totalRecebidoMes,"atb");
$tpl->TOTAL_VENCIDOS = $totalVencidos;
$tpl->INADIMPLENCIA = $inadinplencia;
$rows = $obj->getBoletosAvencer(4);
foreach ($rows as $key => $value) {
    $tpl->GRUPO = $value->controleBoleto->participante->grupo->nomePacote;
    $tpl->PARTICIPANTE = $value->controleBoleto->participante->cliente->nomeCompleto;
    $tpl->VENCIMENTO = $obj->convdata($value->dataVencimento,"mtn");
    $tpl->VALOR = $value->controleBoleto->participante->grupo->moeda->cifrao." ".$obj->money($value->valorMoeda,"atb");
    $tpl->VALOR_MOEDA = $value->valorMoeda;
    $tpl->ID_PARCELA = $value->id;
    $tpl->block("ITEM_BOLETO");
}
foreach ($grupos as $key => $value) {
    $tpl->NOME_GRUPO = $value->nomePacote;
    $tpl->ID_GRUPO = $value->id;
    $tpl->block('LISTA_GRUPO');
}

include("tupi.template.finalizar.php"); 