<?php
include("tupi.inicializar.php");
include("tupi.template.inicializar.php");
$codAcesso = 53;

include("tupi.seguranca.php");

$obGaleria = new Galeria();
$obGrupo = new Grupo();

$id = isset($_REQUEST['id']) ? $obGaleria->md5_decrypt($_REQUEST['id']) : 0;
$aba = isset($_REQUEST['aba']) ? $_REQUEST['aba'] : 0;
$tpl->PUBNAO = 'checked="true"';
$tpl->ACITVE_0 = $aba == 0 ? 'active' : '';
$tpl->ACITVE_1 = $aba == 1 ? 'active' : '';

if($id != 0){

if(!$obGaleria->getById($id)){
    //$obGaleria->getById(1);
    echo "galeria nao encontrada";
    exit();
}
$tpl->ID = $obGaleria->id;
$tpl->NAME = $obGaleria->name;
$tpl->LABEL_GALERIA = $obGaleria->name;
$tpl->PUBSIM =$obGaleria->publish == 1 ? 'checked="true"' : '';
$tpl->PUBNAO =$obGaleria->publish == 0 ? 'checked="true"' : '';


foreach($obGaleria->photos as $key => $value){
    $tpl->FOTO = $value->nameThumb;
    $tpl->ID_FOTO = $value->id;
    $tpl->DESCRIPTION_FOTO = $value->description;
    $tpl->block("BLOCK_FOTO");
}
$tpl->block("BLOCK_EDITAR");
$tpl->block("BLOCK_EDITAR_PILL");
}
$rsGrupos = $obGrupo->getRows();
foreach ($rsGrupos as $key => $value) {
    $tpl->ID_GRUPO = $value->id;
    $tpl->NOME_GRUPO = $value->id."-".$value->nomePacote;
    if($id != 0)
        $tpl->SELECTED_GRUPO = $value->id == $obGaleria->grupo->id ? 'selected' : '';
    $tpl->block('BLOCK_GRUPO');
}



include("tupi.template.finalizar.php"); 