<?php 
include("tupi.inicializar.php");
$newTemplate = 1; 
include("tupi.template.inicializar.php"); 
$codAcesso = 5;
include("tupi.seguranca.php");

$obj = new Cliente();

if(isset($_REQUEST['export'])){
    header('Content-Disposition: attachment;filename="export.csv";');
    header('Content-Type: application/vnd.ms-excel; charset=utf-8');
    $rs = $obj->getRows(0,99999,array("nomeCompleto"=>"ASC"),array());
    echo "NOME;TELEFONE RESIDENCIAL;TELEFONE CELULAR;EMAIL".chr(13);
    foreach ($rs as $key => $value) {
        echo '"'.trim($value->nomeCompleto).'";"'.trim($value->telefoneResidencial).'";"'.trim($value->celular).'";"'.trim($value->email).'"'.chr(13);
    }
    exit();
}

$total = $obj->recuperaTotal('');
$pagina= isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$configPaginacao = $obj->paginar($total,$pagina);
$rows = $obj->pesquisa($configPaginacao['primeiroRegistro'],$configPaginacao['quantidadePorPagina'],'');
foreach ($rows as $key => $value) {
    $tpl->NOME = $value->nomeCompleto;
    $tpl->DATA_NASCIMENTO = $obj->convdata($value->dataNascimento,"mtn");
    $tpl->TELEFONE_RESIDENCIAL = $value->telefoneResidencial;
    $tpl->TELEFONE_CELULAR = $value->celular;
    $tpl->TELEFONE_COMERCIAL = $value->telefoneComercial;
    $tpl->EMAIL = $value->email;
    $tpl->ID_CLIENTE = $value->id;
    $tpl->block("ITEM");
}
$tpl->PAGINA = $pagina;
$tpl->TOTAL_PAGINAS =$configPaginacao['totalPaginas'];
$tpl->PROXIMA_PAGINA =$configPaginacao['proximaPagina'];
$tpl->PAGINA_ANTERIOR =$configPaginacao['paginaAnterior'];

if($configPaginacao['totalPaginas'] > 1){
    $tpl->block("BLOCK_PAGINACAO");
}

include("tupi.template.finalizar.php"); 