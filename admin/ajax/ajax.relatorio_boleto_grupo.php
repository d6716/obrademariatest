<?php
header("Content-Type: text/html; charset=iso-8859-1");
include("../tupi.inicializar.php");
try{
    $obj = new ControleBoletoParcela();
    if($_REQUEST['idgrupo'] == ""){
        throw new Exception("<tr><td colspan='4'><p class='text-center'>N�o foram encontrados registros para essa consulta.</p></td></tr>");
    }

    switch ($_REQUEST['type']) {
        case '1':
            $rs = $obj->getBoletosVencidosPorGrupo($_REQUEST['idgrupo']);
            break;
            case '2':
                $rs = $obj->getBoletosLiquidadosPorGrupo($_REQUEST['idgrupo']);
                break;
                case '3':
                    $rs = $obj->getBoletosAVencerPorGrupo($_REQUEST['idgrupo']);
                    break;
        default:
            # code...
            break;
    }
    
    $result = "";
   
    if(count($rs) > 0){
        $total = 0;
        $totalMoeda = 0;
    foreach ($rs as $key => $value) {
        $result .= "<tr><td>".$value->controleBoleto->participante->cliente->nomeCompleto."</td>
        <td>".$obj->convdata($value->dataVencimento,"mtn")."</td>
        <td class='text-right'>R$ ".$obj->money($value->valorReal,'atb')."</td>
        <td class='text-right'>U$ ".$obj->money($value->valorMoeda,'atb')."</td></tr>";
        $total += $value->valorReal;
        $totalMoeda += $value->valorMoeda;
        
        
    }

    $result .= "<tr><td></td>
    <td class='text-right'><b>Total:</b></td>
    <td class='text-right'><b>R$ ".$obj->money($total,'atb')."</b></td>
    <td class='text-right'><b>U$ ".$obj->money($totalMoeda,'atb')."</b></td></tr>";

}else{
    $result .= utf8_encode("<tr><td colspan='4'><p class='text-center'>N�o foram encontrados registros para essa consulta.</p></td></tr>");
}
    echo json_encode(array("code"=>"200","data"=>utf8_encode($result)));
}catch(Exception $e){
$mensagem = utf8_encode($e->getMessage());
echo json_encode(array("code"=>"200","data"=>"$mensagem"));
}