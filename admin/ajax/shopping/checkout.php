<?php
header("Content-Type: text/html; charset=iso-8859-1");
include("../../tupi.inicializar.php");
$obVenda = new VendaSite();


$obCheckout = new GerenciaNetCheckOut();
$obCielo = new MyCieloCheckout();
$obCliente = new Cliente();
$obGrupo = new Grupo();
$obCidade = new Cidade();
$obParticipante = new Participante();
$obPagamento = new Pagamento();
$obRoteiro = new Roteiro();
$obCheckout->conn->autocommit(false);
$obCheckout->conn->begin_transaction();
try{
    if(!$obVenda->confereConfiguracao()){
        throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
        exit();
    }
//grupo


$_REQUEST = $obCheckout->utf8_decode_array($_REQUEST);



$roteiro = $obRoteiro->getByGrupo($_REQUEST['idGrupo']);
if(count($roteiro)==0){
    throw new Exception("Grupo n�o autorizado");
}
if($roteiro[0]->publish == 0){
    throw new Exception("Grupo n�o autorizado");
    exit();
}

if(!$obGrupo->getById($_REQUEST['idGrupo'])){
    throw new Exception("Grupo n�o encontrado");
}


$dataHoje = Datetime::createFromFormat('Y-m-d',date("Y-m-d"));
$dataEmbarque = Datetime::createFromFormat('Y-m-d',$obGrupo->dataEmbarque);
$interval = $dataHoje->diff($dataEmbarque);
$meses = ($interval->y*12)+$interval->m;
if($obGrupo->bitCheque == 1 && $obGrupo->parcelaCheque != null && $obGrupo->parcelaCheque != '' && $obGrupo->parcelaCheque > 1 ){
    $mesesParcelaCheque = $meses < $obGrupo->parcelaCheque ? $meses : $obGrupo->parcelaCheque;
}else{
    $mesesParcelaCheque = 1;
}
if($obGrupo->bitCartao == 1 && $obGrupo->parcelaCartao != null && $obGrupo->parcelaCartao != '' && $obGrupo->parcelaCartao > 1 ){
    $mesesParcelaCartao =  $obGrupo->parcelaCartao;
}else{
    $mesesParcelaCartao = 1;
}

$formaPagamento = $_REQUEST['forma'];
switch($_REQUEST['forma']){
    case 'formaAVista':
        $tipoPagamento1 = $_REQUEST['pagamentoAVista'];
        $tipoPagamento2 = '';
    break;
    case 'formaEntrada':
        $tipoPagamento1 = $_REQUEST['entradaPagamentoEntrada'];
        $tipoPagamento2 = $_REQUEST['pagamentoEntrada'];   
    break;
    case 'formaParcelado':
        $tipoPagamento1 = $_REQUEST['pagamentoParcelado'];
        $tipoPagamento2 = '';
    break;
    case 'formaOutros':
        $tipoPagamento1 = isset($_REQUEST['pagamentoCustomizado']) ? $_REQUEST['pagamentoCustomizado'] : 'Outros meios de pagamento';
        $tipoPagamento2 = '';
    break;
    case 'formaSomenteAdesao':
        $tipoPagamento1 = isset($_REQUEST['pagamentoSomenteAdesao']) ? $_REQUEST['pagamentoSomenteAdesao'] : 'Outros meios de pagamento';
        $tipoPagamento2 = '';
    break;
    }




//incluir o cliente
$idCliente = $obCliente->saveBySite($_REQUEST);
if($obParticipante->getByIdCliente($idCliente,$obGrupo->id)){
    throw new Exception("N�o � poss�vel adquirir o pacote pois o cliente ".$obCliente->nomeCompleto." j� est� cadastrado neste pacote tur�stico");
}
//incluir como participante no grupo
$idParticipante  = $obParticipante->saveBySite($obGrupo,$obCliente,isset($_REQUEST['opcional'])?1:0,$formaPagamento,$tipoPagamento1);

//outros participantes
$idPartAcomp1 = 0;
$idPartAcomp2 = 0;
$idPartAcomp3 = 0;
$idPartAcomp4 = 0;
if($_REQUEST['quantidade'] > 1){
    if(isset($_REQUEST['acompanhante1'])){
        $obAcomp = new Cliente();
        $obParticipanteAcomp = new Participante();
        $idAcomp1 = $obAcomp->saveAcompanhanteBySite($_REQUEST['acompanhante1'],$_REQUEST['email1'],isset($_REQUEST['mailmarketing']) ? 1 : 0);
        
        if($obParticipante->getByIdCliente($idAcomp1,$obGrupo->id)){
            throw new Exception("N�o � poss�vel adquirir o pacote pois o cliente ".$obAcomp->nomeCompleto." com email: ".$obAcomp->email." j� est� cadastrado neste pacote tur�stico");
        }

        $idPartAcomp1  = $obParticipanteAcomp->saveBySite($obGrupo,$obAcomp,isset($_REQUEST['opcional'])?1:0,$formaPagamento,$tipoPagamento1);

    }
    if(isset($_REQUEST['acompanhante2'])){
        $obAcomp = new Cliente();
        $obParticipanteAcomp = new Participante();
        $idAcomp1 = $obAcomp->saveAcompanhanteBySite($_REQUEST['acompanhante2'],$_REQUEST['email2'],isset($_REQUEST['mailmarketing']) ? 1 : 0);
        
        if($obParticipante->getByIdCliente($idAcomp1,$obGrupo->id)){
            throw new Exception("N�o � poss�vel adquirir o pacote pois o cliente ".$obAcomp->nomeCompleto." com email: ".$obAcomp->email." j� est� cadastrado neste pacote tur�stico");
        }

        $idPartAcomp2  = $obParticipanteAcomp->saveBySite($obGrupo,$obAcomp,isset($_REQUEST['opcional'])?1:0,$formaPagamento,$tipoPagamento1);

    }
    if(isset($_REQUEST['acompanhante3'])){
        $obAcomp = new Cliente();
        $obParticipanteAcomp = new Participante();
        $idAcomp1 = $obAcomp->saveAcompanhanteBySite($_REQUEST['acompanhante3'],$_REQUEST['email3'],isset($_REQUEST['mailmarketing']) ? 1 : 0);

        if($obParticipante->getByIdCliente($idAcomp1,$obGrupo->id)){
            throw new Exception("N�o � poss�vel adquirir o pacote pois o cliente ".$obAcomp->nomeCompleto." com email: ".$obAcomp->email." j� est� cadastrado neste pacote tur�stico");
        }

        $idPartAcomp3  = $obParticipanteAcomp->saveBySite($obGrupo,$obAcomp,isset($_REQUEST['opcional'])?1:0,$formaPagamento,$tipoPagamento1);

    }
    if(isset($_REQUEST['acompanhante4'])){
        $obAcomp = new Cliente();
        $obParticipanteAcomp = new Participante();
        $idAcomp1 = $obAcomp->saveAcompanhanteBySite($_REQUEST['acompanhante4'],$_REQUEST['email4'],isset($_REQUEST['mailmarketing']) ? 1 : 0);

        if($obParticipante->getByIdCliente($idAcomp1,$obGrupo->id)){
            throw new Exception("N�o � poss�vel adquirir o pacote pois o cliente ".$obAcomp->nomeCompleto." com email: ".$obAcomp->email." j� est� cadastrado neste pacote tur�stico");
        }

        $idPartAcomp4  = $obParticipanteAcomp->saveBySite($obGrupo,$obAcomp,isset($_REQUEST['opcional'])?1:0,$formaPagamento,$tipoPagamento1);

    }
}






$idVenda = $obVenda->createVenda($obParticipante,$obGrupo,isset($_REQUEST['opcional'])?1:0,$_REQUEST['quantidade'],$formaPagamento,$tipoPagamento1,$tipoPagamento2,$idPartAcomp1,$idPartAcomp2,$idPartAcomp3,$idPartAcomp4);
$textFormaPagamento = "<b>Forma de Pagamento Escolhida:</b><br/>";
switch($_REQUEST['forma']){
 case 'formaOutros':
    $textFormaPagamento .= $obGrupo->nomeCustomizado."<br/>".$obGrupo->textCustomizado;
    if($obGrupo->bitAdesaoCustomizado == 1){
        $textFormaPagamento .= "Ades�o paga em:<br/>";
        $valorPagamento = 0;
        if($obVenda->opcional == 1){
            $valorPagamento = ($obGrupo->valorAdesao+$obGrupo->valorAdesaoOpcional)*$obVenda->cotacao;
          }else{
            $valorPagamento = ($obGrupo->valorAdesao+$obGrupo->valorAdesaoOpcional)*$obVenda->cotacao;
          }
       
        if($tipoPagamento1 == 'credit_card')
        {
            $textFormaPagamento .= "Cart�o de Cr�dito <br/>";
            $response = $obCielo->createLinkPagamento($obParticipante,$obGrupo,$obVenda,$valorPagamento,1);
            $linkCartao = $response->settings->checkoutUrl;
        } else if($tipoPagamento1 == 'boleto'){
            $textFormaPagamento .= "Boleto Banc�rio <br/>";
            $chargeBoleto = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,$valorPagamento);
            $linkBoleto = $obCheckout->createLinkPagamento($chargeBoleto['data']['charge_id'],'','banking_billet');
    
        }else if($tipoPagamento1 == 'transferencia'){
            $textFormaPagamento .= "Transfer�ncia Banc�ria <br/>";
           
        }
        
        else{
            throw new Exception("Tipo de Pagamento n�o encontrado");
        }
    }
break;  
case 'formaSomenteAdesao':
    if($obGrupo->bitSomenteAdesao == 1){
        $textFormaPagamento .= "Ades�o paga em:<br/>";
        $valorPagamento = 0;
        if($obVenda->opcional == 1){
            $valorPagamento = ($obGrupo->valorAdesao+$obGrupo->valorAdesaoOpcional)*$obVenda->cotacao;
          }else{
            $valorPagamento = ($obGrupo->valorAdesao+$obGrupo->valorAdesaoOpcional)*$obVenda->cotacao;
          }
       
        if($tipoPagamento1 == 'credit_card')
        {
            $textFormaPagamento .= "Cart�o de Cr�dito <br/>";
            $response = $obCielo->createLinkPagamento($obParticipante,$obGrupo,$obVenda,$valorPagamento,1);
            $linkCartao = $response->settings->checkoutUrl;
        } else if($tipoPagamento1 == 'boleto'){
            $textFormaPagamento .= "Boleto Banc�rio <br/>";
            $chargeBoleto = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,$valorPagamento);
            $linkBoleto = $obCheckout->createLinkPagamento($chargeBoleto['data']['charge_id'],'','banking_billet');
    
        }else if($tipoPagamento1 == 'transferencia'){
            $textFormaPagamento .= "Transfer�ncia Banc�ria <br/>";
           
        }
        
        else{
            throw new Exception("Tipo de Pagamento n�o encontrado");
        }
    }else{
        throw new Exception("Tipo de Pagamento n�o encontrado");
    }
break;
case 'formaAVista':
    $textFormaPagamento .= '� Vista <br/>';
    $valorPagamento = $obVenda->total;
    if($_REQUEST['pagamentoAVista'] == 'transferencia'){
        $textFormaPagamento .= 'Transfer�ncia Banc�ria <br/>';
        //$obVenda->incluirPagamentoSiteTransferencia($valorPagamento,"Pagamentos pela internet a vista");
    } else if($_REQUEST['pagamentoAVista'] == 'boleto'){
        $textFormaPagamento .= 'Boleto Banc�rio <br/>';
        $chargeBoleto = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,$valorPagamento);
        $linkBoleto = $obCheckout->createLinkPagamento($chargeBoleto['data']['charge_id'],'','banking_billet');

    }elseif($_REQUEST['pagamentoAVista'] == 'cheque'){
        $textFormaPagamento .= 'Cheque <br/>';
        //$obVenda->incluirPagamentoSiteCheque($valorPagamento,date("Y-m-d"),"Pagamentos pela internet a vista");
    }elseif($_REQUEST['pagamentoAVista'] == 'credit_card')
    {
        $textFormaPagamento .= 'Cart�o de Cr�dito <br/>';
         //$obCheckout = new GerenciaNetCheckOut();
        //$chargeCartao = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,$obVenda->total/$obVenda->quantidade);
        //$linkCartao = $obCheckout->createLinkPagamento($chargeCartao['data']['charge_id'],'','credit_card');
        $response = $obCielo->createLinkPagamento($obParticipante,$obGrupo,$obVenda,$obVenda->total/$obVenda->quantidade,1);
        $linkCartao = $response->settings->checkoutUrl;
    }else{
        throw new Exception("Tipo de Pagamento n�o encontrado");
    }
break;
case 'formaEntrada':
    $textFormaPagamento .= 'Entrada de '.($_REQUEST['percentualEntrada']*100).'%:<br/>';
    $valorEntrada = ($obVenda->total)*$_REQUEST['percentualEntrada'];
    $valorResto = ($obVenda->total)*(1-$_REQUEST['percentualEntrada']);
    $obVenda->percentualEntrada = $_REQUEST['percentualEntrada'];
    $obVenda->valorEntrada = $obVenda->money($valorEntrada,"bta");
    $obVenda->valorResto = $obVenda->money($valorResto,"bta");
    $obVenda->save();
    if($_REQUEST['entradaPagamentoEntrada'] == 'transferencia'){
        $textFormaPagamento .= 'Transfer�ncia Banc�ria <br/>';
        //$obVenda->incluirPagamentoSiteTransferencia($valorEntrada,"Pagamentos pela internet entrada");
    }elseif($_REQUEST['entradaPagamentoEntrada'] == 'boleto'){
        $textFormaPagamento .= 'Boleto Banc�rio <br/>';
        $chargeBoleto = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,($valorEntrada/$obVenda->quantidade));
        $linkBoleto = $obCheckout->createLinkPagamento($chargeBoleto['data']['charge_id'],'','banking_billet');

    }elseif($_REQUEST['entradaPagamentoEntrada'] == 'cheque'){
        $textFormaPagamento .= 'Cheque <br/>';
        //$obVenda->incluirPagamentoSiteCheque($valorEntrada, date("Y-m-d"),"Pagamentos pela internet entrada");
    }else{
        throw new Exception("Tipo de Pagamento n�o encontrado");
    }
    $textFormaPagamento .= " Restante parcelado em:<br/>";
    //parcelado
    if($_REQUEST['pagamentoEntrada'] == 'credit_card'){
        $textFormaPagamento .= 'Cart�o de Cr�dito';
        $valorParcela = $valorResto/$mesesParcelaCartao;
        //$obCheckout = new GerenciaNetCheckOut();
        //$chargeCartao = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,$valorResto/$obVenda->quantidade);
        //$linkCartao = $obCheckout->createLinkPagamento($chargeCartao['data']['charge_id'],'','credit_card');
        $response = $obCielo->createLinkPagamento($obParticipante,$obGrupo,$obVenda,$valorResto/$obVenda->quantidade,$mesesParcelaCartao);
        $linkCartao = $response->settings->checkoutUrl;

    }elseif($_REQUEST['pagamentoEntrada'] == 'cheque'){
        $textFormaPagamento .= 'Cheque';
        $valorParcela = $valorResto/$mesesParcelaCheque;
        /*for($i=1;$i<=$mesesParcelaCheque;$i++){
            $dataAtual = DateTime::createFromFormat("Y-m-d",date("Y-m-d"));
            $dataAtual->add(new DateInterval("P".$i."M"));
            $obVenda->incluirPagamentoSiteCheque($valorParcela,$dataAtual->format("Y-m-d"),"Pagamentos pela internet parcelamento");
        }*/
    }else{
        throw new Exception("Tipo de Pagamento n�o encontrado");
    }

break;
case 'formaParcelado':
    $textFormaPagamento .= 'Parcelado em:<br/>';
    if($_REQUEST['pagamentoParcelado'] == 'credit_card')
    {
        $textFormaPagamento .= 'Cart�o de Cr�dito';
        $valorParcela = $obVenda->total/$mesesParcelaCartao;
        //$obCheckout = new GerenciaNetCheckOut();
        //$chargeCartao = $obCheckout->createCharge($obParticipante,$obGrupo,$obVenda,$obVenda->total/$obVenda->quantidade);
        //$linkCartao = $obCheckout->createLinkPagamento($chargeCartao['data']['charge_id'],'','credit_card');
        $response = $obCielo->createLinkPagamento($obParticipante,$obGrupo,$obVenda,$obVenda->total/$obVenda->quantidade,$mesesParcelaCartao);
        $linkCartao = $response->settings->checkoutUrl;
    }elseif($_REQUEST['pagamentoParcelado'] == 'cheque'){
        $textFormaPagamento .= 'Cheque';
        $valorParcela = ($obVenda->total)/$mesesParcelaCheque;
       /* for($i=1;$i<=$mesesParcelaCheque;$i++){
            $dataAtual = DateTime::createFromFormat("Y-m-d",date("Y-m-d"));
            $dataAtual->add(new DateInterval("P".$i."M"));
            $obVenda->incluirPagamentoSiteCheque($valorParcela,$dataAtual->format("Y-m-d"),"Pagamentos pela internet parcelado");
        }*/
    }
    
break;
}


if($obCheckout->conn->commit()){

    //enviando email com dados da compra:
    $html = "Parab�ns peregrino ".$_REQUEST['nomeCompleto'].", bem vindo � Obra de Maria DF!<br/><br/>";
    $html .= "Voc� est� recebendo este email porque acabou de se inscrever em nosso roteiro de peregrina��o : ".$obGrupo->nomePacote.".<br/><br/>";
    $html .= "Sua inscri��o foi realizada com sucesso!</br>Para dar continuidade, clique no link abaixo para realizar o pagamento ou entre em contato conosco!<br/>";
    $html .= "<a href='".$obGrupo->urlSite."/bilhete.php?charge_id=".$obVenda->id."'>Acessar minha reserva</a>";
    $html .= "<a href='".$obGrupo->endpointcn."free/pdf/assinado/1/".$obParticipante->id.".pdf'>Acessar meu contrato</a>";
    if($obGrupo->roteiroAnexo != ""){
    $html .= "<br/><a href='".$obGrupo->urlSite."docs/".$obGrupo->roteiroAnexo."'>Acessar roteiro detalhado</a>";
    }


    //ROTEIRO 
    $roteiros = $obRoteiro->getByGrupo($obGrupo->id);
    if(count($roteiros) > 0){
    $html .= "<h4>Roteiro do seu pacote tur�stico:</h4>";
    $roteiro = $roteiros[0];
    foreach($roteiro->itineraryes as $key => $it){
        $html .= $it->order."-".$it->title."<br/>";
        $html .= $it->description."<br/><br/>";
    }
    }

    $tplemail = new Template("../../templates/tpl_email_ecommerce.html");
    $tplemail->CONTEUDO = $html;
    $obVenda->mail_html($_REQUEST['email'],$obVenda->REMETENTE, 'Obra de Maria DF - '.str_pad($obVenda->id,10,"0",STR_PAD_LEFT), $tplemail->showString());
    


    //email para a obra de maria
    //enviando email com dados da compra:
    $html = "VENDA REALIZADA PELO SITE  ".$_REQUEST['nomeCompleto']."-".$obCliente->cpf."<br/><br/>";
    $html .= "Grupo: ".$obGrupo->nomePacote.".<br/><br/>";
    $html .= "Data do Embarque: ".$obGrupo->convdata($obGrupo->dataEmbarque,"mtn").".<br/><br/>";
    $html .= "Forma de Pagamento: ".$obVenda->printFormaPagamento()."<br/><br/>";
    $html .= "Tipo de Pagamento: ".$obVenda->printTipoPagamento()."<br/><br/>";
    
    $tplemail = new Template("../../templates/tpl_email_ecommerce.html");
    $tplemail->CONTEUDO = $html;
    $obVenda->mail_html($obVenda->DESTINATARIO,$obVenda->REMETENTE, 'Obra de Maria DF - '.str_pad($obVenda->id,10,"0",STR_PAD_LEFT), $tplemail->showString());
    $obCheckout->conn->begin_transaction();
    //gerar o contrato no mydocs do participante 1:
    $obParticipante->gerarContratoMyDocsSite($textFormaPagamento);
    $obCheckout->conn->commit();
    echo json_encode(array("code"=>"200","data"=>array("charge_id"=>$idVenda)));
}else{
    echo json_encode(array("code"=>"500","data"=>array("mensagem"=>"Erro interno, tente novamente mais tarde")));
}

}catch(Exception $e){    
    $mensagem = utf8_encode($e->getMessage());
    echo json_encode(array("code"=>"500","data"=>array("mensagem"=>"$mensagem")));
    $obCheckout->conn->rollback();
    exit();
}