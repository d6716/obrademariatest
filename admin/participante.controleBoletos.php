<?php 
include("tupi.inicializar.php");
$newTemplate = 1; 
include("tupi.template.inicializar.php"); 
$codAcesso = 13;
include("tupi.seguranca.php");

$obj = new Participante();
$oCB = new ControleBoleto();
$oCBP = new ControleBoletoParcela();
$oAg = new Agendamento();
$oAg->getById(6);
$id = $obj->md5_decrypt($_REQUEST['idParticipante']);
$obj->getById($id);
$tpl->COTACAO_JAVAS = $obj->grupo->moeda->id == 2 ? 1.00 : $oAg->destinatarios;
$tpl->VALOR_MOEDA_JAVAS = 0;
$tpl->CIFRAO = $obj->grupo->moeda->cifrao;
$tpl->COTACAO =   $obj->grupo->moeda->id == 2 ? '1,00' : $oAg->money($oAg->destinatarios,"atb");
$tpl->MOEDA_IS_REAL = $obj->grupo->moeda->id == 2 ? 'readonly' : '';

if($oCB->getRow(array("participante"=>"=".$id))){
    $tpl->VALOR_MOEDA_JAVAS = $oCB->money($oCB->valorTotal/$oCB->numeroParcelas,"bta");
    $tpl->DATA_CRIACAO = $oCB->convdata($oCB->dataCriacao,"mtnh");
    $tpl->DIA_VENCIMENTO = $oCB->diaVencimento;
    $tpl->PARCELAS = $oCB->numeroParcelas;
    $tpl->VALOR = $oCB->grupo->moeda->cifrao." ".$oCB->money($oCB->valorTotal,"atb");
    $tpl->block("BLOCK_CONTROLE");

    $boletos = $oCBP->getRows(0,999,array("numero"=>"ASC"),array("controleBoleto"=>"=".$oCB->id));
    $i = 1;
    foreach ($boletos as $key => $boleto) {
        $tpl->NUMERO = $boleto->numero;
        $tpl->VENCIMENTO = $oCB->convdata($boleto->dataVencimento,"mtn");
        $tpl->VALOR_MOEDA = $oCB->money($boleto->valorMoeda,"atb");
        $tpl->VALOR_REAL = $oCB->money($boleto->valorReal,"atb");
        $tpl->COTACAO_PARCELA = $oCB->money($boleto->cotacao,"atb");

        
        
        if($boleto->isGerado == 1){
            $tpl->URL_BOLETO = $boleto->gnUrlBoleto;
            $tpl->block("BLOCK_GERADO");
        }else{
            $tpl->ID_PARCELA = $boleto->id;
            $tpl->block("BLOCK_NAO_GERADO");
        }
        $tpl->block("BLOCK_BOLETO");
        $i++;
    }
    $tpl->block("BLOCK_BOLETOS");
}else{
    $tpl->block("BLOCK_NOVO");
}

$tpl->GRUPO = $obj->grupo->nomePacote;
$tpl->NOME_PARTICIPANTE = $obj->cliente->nomeCompleto;
$tpl->MOEDA = $obj->grupo->moeda->cifrao;
$tpl->ID_PARTICIPANTE = $obj->id;
include("tupi.template.finalizar.php"); 