<?php
include("tupi.inicializar.php");
include("tupi.template.inicializar.php");
$codAcesso = 13;
include("tupi.seguranca.php");
if(!isset($_REQUEST['ajax'])){
$tpl->BREADCRUMB = '    <ul class="breadcrumb">
    <li>
    <a href="home.php">Home</a> <span class="divider">/</span>
    </li>
    <li>
    <a href="grupos.andamento.php">Grupos</a> <span class="divider">/</span>
    </li>

    <li class="active">Lista de Espera do grupo</li>
    </ul>';
}
//configura o grupo na pagina
$oGrupo = new Grupo();
$idGrupo = $oGrupo->md5_decrypt($_REQUEST['idGrupo']);
$oGrupo->getById($idGrupo);
$tpl->NOME_GRUPO = $oGrupo->nomePacote;
$tpl->ID_GRUPO_HASH = $_REQUEST['idGrupo'];
$obj = new ListaEspera();
$strBusca = isset($_REQUEST['busca']) ? str_replace(".","",str_replace("-","",$_REQUEST['busca'])) : "";
$total = $obj->recuperaTotal($idGrupo,$strBusca);
$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$configPaginacao = $obj->paginar($total,$pagina);
$rs = $obj->pesquisa($configPaginacao['primeiroRegistro'],$configPaginacao['quantidadePorPagina'],$idGrupo,$strBusca);
if($configPaginacao['totalPaginas'] > 1){
$tpl->block("BLOCK_PAGINACAO");
}
$tpl->BUSCA = $strBusca;
$tpl->TOTAL_PAGINAS = $configPaginacao['totalPaginas'];
$tpl->PAGINA_ANTERIOR = $configPaginacao['paginaAnterior'];
$tpl->PROXIMA_PAGINA = $configPaginacao['proximaPagina'];
$tpl->PAGINA = $pagina;
$i = ($configPaginacao['quantidadePorPagina']*($pagina-1))+1;
foreach($rs as $key => $lista){
    $tpl->ID_HASH = $obj->md5_encrypt($lista->id);
    $tpl->NUMERO = $i;
	$tpl->EMAIL = $lista->email;
	$tpl->TELEFONE = $obj->formataTelefone($lista->telefone);
	$tpl->NOME =$lista->nome;
	//calcula o status dos pagamentos;
	$i++;
$tpl->block("BLOCK_ITEM_LISTA");
}
include("tupi.template.finalizar.php");
?>