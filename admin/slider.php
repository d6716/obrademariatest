<?php
include("tupi.inicializar.php");
include("tupi.template.inicializar.php");
$codAcesso = 52;

include("tupi.seguranca.php");

$obj = new Slide();

$total = $obj->getRowsCount();

$configPaginacao = $obj->paginar($total,isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1);
$rs = $obj->getRows($configPaginacao['primeiroRegistro'],$configPaginacao['quantidadePorPagina'],array(),array());	

if($configPaginacao['totalPaginas'] > 1){
$tpl->PAGINA = $configPaginacao['pagina'];
$tpl->TOTAL_PAGINAS = $configPaginacao['totalPaginas'];
$tpl->PAGINA_ANTERIOR = $configPaginacao['paginaAnterior'];
$tpl->PROXIMA_PAGINA = $configPaginacao['proximaPagina'];
$tpl->block("BLOCK_PAGINACAO");
}


foreach ($rs as $key => $value) {
    $tpl->ID = $value->id;
    $tpl->ID_HASH = $obj->md5_encrypt($value->id);
    $tpl->NOME = $value->title;
    $tpl->LABEL = $value->publish == 1 ? 'success' : 'warning';
    $tpl->PUB  = $value->publish == 1 ? 'SIM' : 'N�O';
    $tpl->block('BLOCK_ITEM_LISTA');
}

include("tupi.template.finalizar.php"); 