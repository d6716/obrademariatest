<?php
include("tupi.inicializar.php");
include("tupi.template.inicializar.php");
$codAcesso = 52;

include("tupi.seguranca.php");

$obSlider = new Slide();
$obRoteiro = new Roteiro();
$obGrupo = new Grupo();
$obFoto = new Foto();
$obVideo = new Video();
$obEtinerario = new Etinerario();

$id = isset($_REQUEST['id']) ? $obSlider->md5_decrypt($_REQUEST['id']) : 0;
$tpl->PUBNAO = 'checked="true"';
$tpl->IMAGE = 'placeholder.jpg';
if($id != 0){
if(!$obSlider->getById($id)){
    //$obRoteiro->getById(1);
    echo "slide nao encontrado";
    exit();
}
$tpl->LABEL_SLIDER = $obSlider->title;
$tpl->ID = $obSlider->id;
$tpl->TITLE = $obSlider->title;
$tpl->SUB_TITLE = $obSlider->subTitle;
$tpl->DESCRIPTION = $obSlider->description;
$tpl->TEXT_BUTTOM = $obSlider->buttomText;
$tpl->IMAGE = $obSlider->image;
$tpl->PUBSIM = $obSlider->publish == 1 ? 'checked="true"' : '' ;
$tpl->PUBNAO = $obSlider->publish == 0 ? 'checked="true"' : '' ;
}
$rsRoteiros = $obRoteiro->getRoteirosSemSlider();
if($id != 0){
    array_push($rsRoteiros,$obSlider->roteiro);
}

foreach ($rsRoteiros as $key => $value) {
    $tpl->ID_ROTEIRO = $value->id;
    $tpl->TITLE_ROTEIRO = $value->cardTitle;
    if($id != 0)
        $tpl->SELECTED_ROTEIRO = $value->id == $obSlider->roteiro->id ? 'selected' : '';
    $tpl->block('BLOCK_ROTEIRO');
}



include("tupi.template.finalizar.php"); 