<?php
class ItemObraDeMaria extends Persistencia {
    var $ordem;
    var $texto;
    var $foto;
    
    function getFolder(){
        return str_replace("admin/","",$this->URI)."img/reviews/";
    }

    function excluir(){
        if($this->foto!= null && $this->foto != '')
            $this->apagaImagem($this->foto,$this->getFolder());
        $this->delete($this->id);
    }

    function salva($file,$ordem,$texto){

        $this->texto = $texto;            
        $this->ordem = $ordem;


    if($file['name'] != ''){      
        
        if($this->foto!= null && $this->foto != '')
            $this->apagaImagem($this->foto,$this->getFolder());

        $names = explode(".",$file['name']);
        $nomefoto = $this->retornaNomeUnico("obrademaria.".date('YmdHis').'.'.$names[count($names)-1],$this->getFolder());
        $this->uploadArquivo($file,$nomefoto,$this->getFolder());
        $this->foto = $nomefoto;
        //$this->resizeImage($this->getFolder(),$nomefoto,300,300);

        
    }
    $this->save();
}
}