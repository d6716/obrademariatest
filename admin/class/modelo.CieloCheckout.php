<?php
use CieloCheckout\Order;
use CieloCheckout\Item;
use CieloCheckout\Discount;
use CieloCheckout\Cart;
use CieloCheckout\Address;
use CieloCheckout\Services;
use CieloCheckout\Shipping;
use CieloCheckout\Payment;
use CieloCheckout\Customer;
use CieloCheckout\Options;
use CieloCheckout\Transaction;
use Cielo\Merchant;

 /*
        "order_number": "Pedido01",
        "amount": 101,
        "discount_amount": 0,
        "checkout_cielo_order_number": "65930e7460bd4a849502ed14d7be6c03",
        "created_date": "12-09-2017 14:38:56",
        "customer_name": "Test Test",
        "customer_phone": "21987654321",
        "customer_identity": "84261300206",
        "customer_email": "test@cielo.com.br",
        "shipping_type": 1,
        "shipping_name": "Motoboy",
        "shipping_price": 1,
        "shipping_address_zipcode": "21911130",
        "shipping_address_district": "Freguesia",
        "shipping_address_city": "Rio de Janeiro",
        "shipping_address_state": "RJ",
        "shipping_address_line1": "Rua Cambui",
        "shipping_address_line2": "Apto 201",
        "shipping_address_number": "92",
        "payment_method_type": 1,
        "payment_method_brand": 1,
        "payment_maskedcreditcard": "471612******7044",
        "payment_installments": 1,
        "payment_status": 3,
        "tid": "10447480686J51OH8BPB",
        "test_transaction": "False"
    
    1 	Pendente 	Para todos os meios de pagamento 	Indica que o pagamento ainda est� sendo processado; OBS: Boleto - Indica que o boleto n�o teve o status alterado pelo lojista
    2 	Pago 	Para todos os meios de pagamento 	Transa��o capturada e o dinheiro ser� depositado em conta.
    3 	Negado 	Somente para Cart�o Cr�dito 	Transa��o n�o autorizada pelo respons�vel do meio de pagamento
    4 	Expirado 	Cart�es de Cr�dito e Boleto 	Transa��o deixa de ser v�lida para captura - 15 dias p�s Autoriza��o
    5 	Cancelado 	Para cart�es de cr�dito 	Transa��o foi cancelada pelo lojista
    6 	N�o Finalizado 	Todos os meios de pagamento 	Pagamento esperando Status - Pode indicar erro ou falha de processamento. Entre em contato com o Suporte cielo
    7 	Autorizado 	somente para Cart�o de Cr�dito 	Transa��o autorizada pelo emissor do cart�o. Deve ser capturada para que o dinheiro seja depositado em conta
    8 	Chargeback 	somente para Cart�o de Cr�dito 	Transa��o cancelada pelo consumidor junto ao emissor do cart�o. O Dinheiro n�o ser� depositado em conta.

    cartoes
    1 	Visa
    2 	Mastercad
    3 	AmericanExpress
    4 	Diners
    5 	Elo
    6 	Aura
    7 	JCB
    8 	Discover
    9 	Hipercard
    */

class MyCieloCheckout extends Persistencia {
    var $checkoutUrl;
    var $profile;
    var $version;
    var $venda = NULL;
    var $participante = NULL;
    var $amount;
    var $created_date;
    var $checkout_cielo_order_number;
    var $payment_method_type;
    var $payment_method_brand;
    var $payment_method_bank;
    var $payment_maskedcreditcard;
    var $payment_installments;
    var $payment_status;
    var $tid;
    var $test_transaction;
    
    function getStatus(){
      switch ($this->payment_status) {
        case 1:
          return 'Iniciado';
          break;
          case 2:
            return 'Pago';
            break;
            case 3:
              return 'Rejeitado';
              break;
        default:
        return 'N�o Identificado';
          break;
      }
     
  }

    public function getByChargeId($chargeId){
        return $this->getRow(array("checkout_cielo_order_number"=>"=".$chargeId));
    }
    
    public function getByIdWithParticipante($chargeId){
      return $this->getRow(array("id"=>"=".$chargeId,"venda"=>"is null"));
    }

    public function getByParticipanteId($participanteId){
      return $this->getRows(0,99,array(),array("participante"=>"=".$participanteId));
    }
    
    public function getByVendasId($vendaId){
        return $this->getRows(0,4,array(),array("venda"=>"=".$vendaId,"participante"=>"is null"));
    }
    
    public function getByVendasNaoPagasId($vendaId){
        return $this->getRows(0,4,array(),array("venda"=>"=".$vendaId,"payment_status"=>"!=2"));
    }

    function createLinkPagamento($obParticipante,$obGrupo,$obVenda,$valor,$mesesParcelaCartao){
        try {
          if(!$this->confereConfiguracao()){
            throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
            exit();
          }

            $telefone = substr($this->limpaDigitos($obParticipante->cliente->celular != "" ? $obParticipante->cliente->celular : $obParticipante->cliente->telefoneResidencial),0,11);
            if(strlen($telefone) < 10 ||  strlen($telefone) >11){
              throw new Exception("Telefone do cliente inv�lido, deve conter de 10 a 11 d�gitos!");
            }

            if($obVenda->opcional)
                $nomeItem = $obGrupo->nomePacote."+".$obGrupo->nomePacoteOpcional;
                else
                $nomeItem = $obGrupo->nomePacote;
            // Instantiate cart's item object and set it to an array of product items.
            $properties = [
              'Name' => utf8_encode(substr($nomeItem,0,128)),
              'Description' => utf8_encode(substr($obGrupo->destino,0,256)),
              'UnitPrice' => intval(str_replace(".","",str_replace(",","",$this->money($valor,"atb")))),
              'Quantity' => intval($obVenda->quantidade),
              'Type' => 'Service',
              'Sku' => '',
              'Weight' => 0,
            ];
            $Items = [
              new Item($properties),
            ];
            
            // Instantiate cart discount object.
            $properties = [
              'Type' => 'Percent',
              'Value' => 0,
            ];
            $Discount = new Discount($properties);
            
            // Instantiate shipping address' object.
            $properties = [
              'Street' => utf8_encode($obParticipante->cliente->endereco),
              'Number' => "000",
              'Complement' => '',
              'District' => utf8_encode($obParticipante->cliente->bairro),
              'City' => utf8_encode($obParticipante->cliente->cidadeEndereco),
              'State' => utf8_encode($obParticipante->cliente->estadoEndereco),
            ];
            $Address = new Address($properties);
            
            // Instantiate shipping services' object.
            //$properties = [
            //  'Name' => 'Servi�o de frete',
            //  'Price' => 123,
            //  'DeadLine' => 15,
            //];
            
            $Services = [
              //new Services($properties),
            ];
            
            // Instantiate shipping's object.
            $properties = [
              'Type' => 'WithoutShipping',
              'SourceZipCode' => '70000000',
              'TargetZipCode' => str_pad($obParticipante->cliente->cep,8,"0",STR_PAD_RIGHT),
              'Address' => $Address,
              'Services' => $Services,
            ];
            $Shipping = new Shipping($properties);
            
            // Instantiate payment's object.
            $properties = [
              'BoletoDiscount' => 0,
              'DebitDiscount' => 0,
              'MaxNumberOfInstallments' => intval($mesesParcelaCartao),              
            ];
            $Payment = new Payment($properties);
            
            // Instantiate customer's object.
            $properties = [
              'Identity' => $obParticipante->cliente->cpf,
              'FullName' => utf8_encode($obParticipante->cliente->nomeCompleto),
              'Email' => $obParticipante->cliente->email,
              'Phone' => $telefone,
            ];
            $Customer = new Customer($properties);
            
            // Instantiate options' object.
            $properties = [
              'AntifraudEnabled' => TRUE,
              'ReturnUrl' => $this->urlSite."bilhete.php?returnSuccess=1&charge_id=".$obVenda->id,
            ];
            $Options = new Options($properties);
            
            // Instantiate order's object.
            $properties = [
              'OrderNumber' => $obVenda->id,
              'SoftDescriptor' => 'RAINHATOUR',
              // Instantiate cart's object.
              'Cart' => new Cart(['Discount' => $Discount, 'Items' => $Items]),
              'Shipping' => $Shipping,
              'Payment' => $Payment,
              'Customer' => $Customer,
              'Options' => $Options,
            ];
            $Order = new Order($properties);
            $headers = array('Accept' => 'application/json','MerchantId'=>$this->cieloClientID,'Content-Type'=>'application/json; charset=utf-8');
            $query = Unirest\Request\Body::json($Order);
            $response = Unirest\Request::post($this->endpointCielo.'/orders', $headers, $query);

            if($response->code != 200 && $response->code != 201){
                if(isset($response->body)){
                throw new Exception($response->body->message);
                }else{
                    throw new Exception("Erro inesperado!");    
                }
            }

            if(isset($response->body->message)){
                throw new Exception($response->body->message);
            }

            $Transaction = $response->body;
            // Instantiate merchant's object.
            //$Merchant = new Merchant($this->cieloClientID, $this->cieloClientSecret);
            
            // Instantiate transaction's object.
            //$Transaction = new Transaction($Merchant, $Order);
            //$Transaction->request_new_transaction(true);
            

            $this->venda = $obVenda;
            
            $this->payment_status  = 1;
            $this->amount = $this->money($this->money($valor,"atb"),"bta");          
            $this->created_date = date("Y-m-d H:i:s");
            $this->checkoutUrl =  $Transaction->settings->checkoutUrl;
            $this->profile =  $Transaction->settings->profile;
            $this->version =  $Transaction->settings->version;
            $this->payment_installments = $mesesParcelaCartao;
            $this->save();
            //enviar o boleto para verificacao por email:
          @$this->mail_html($this->REMETENTE,'','CART�O INTERNET CIELO CRIADO','<a href="'. $Transaction->settings->checkoutUrl.'">'.$Transaction->settings->checkoutUrl);
            return $Transaction;
            //print_r($Transaction->response);
            
            // This will throw an exception when running from terminal cli.
            //$Transaction->redirect_to_cielo();
          }
          catch(Exception $e) {
            throw $e;
          }
    }

   function createLinkPagamentoParticipante($obParticipante,$valor,$mesesParcelaCartao){
      try {

        if(!$this->confereConfiguracao()){
          throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
          exit();
        }

        $this->conn->autocommit(false);
        $this->conn->begin_transaction();

        $this->participante = $obParticipante;          
        $this->payment_status  = 1;
        $this->amount = $this->money($this->money($valor,"atb"),"bta");          
        $this->created_date = date("Y-m-d H:i:s");
        $orderNumber = "A".$this->save();



          $telefone = substr($this->limpaDigitos($obParticipante->cliente->celular != "" ? $obParticipante->cliente->celular : $obParticipante->cliente->telefoneResidencial),0,11);
          if(strlen($telefone) < 10 ||  strlen($telefone) >11){
            throw new Exception("Telefone do cliente inv�lido, deve conter de 10 a 11 d�gitos!");
          }
          $nomeItem = $obParticipante->grupo->nomePacote;
          $properties = [
            'Name' => utf8_encode(substr($nomeItem,0,128)),
            'Description' => utf8_encode(substr($obParticipante->grupo->destino,0,256)),
            'UnitPrice' => intval(str_replace(".","",str_replace(",","",$this->money($valor,"atb")))),
            'Quantity' => 1,
            'Type' => 'Service',
            'Sku' => '',
            'Weight' => 0,
          ];
          $Items = [
            new Item($properties),
          ];
          
          // Instantiate cart discount object.
          $properties = [
            'Type' => 'Percent',
            'Value' => 0,
          ];
          $Discount = new Discount($properties);
          
          // Instantiate shipping address' object.
          $properties = [
            'Street' => utf8_encode($obParticipante->cliente->endereco),
            'Number' => "000",
            'Complement' => '',
            'District' => utf8_encode($obParticipante->cliente->bairro),
            'City' => utf8_encode($obParticipante->cliente->cidadeEndereco),
            'State' => utf8_encode($obParticipante->cliente->estadoEndereco),
          ];
          $Address = new Address($properties);
          
          // Instantiate shipping services' object.
          //$properties = [
          //  'Name' => 'Servi�o de frete',
          //  'Price' => 123,
          //  'DeadLine' => 15,
          //];
          
          $Services = [
            //new Services($properties),
          ];
          
          // Instantiate shipping's object.
          $properties = [
            'Type' => 'WithoutShipping',
            'SourceZipCode' => '70000000',
            'TargetZipCode' => str_pad($obParticipante->cliente->cep,8,"0",STR_PAD_RIGHT),
            'Address' => $Address,
            'Services' => $Services,
          ];
          $Shipping = new Shipping($properties);
          
          // Instantiate payment's object.
          $properties = [
            'BoletoDiscount' => 0,
            'DebitDiscount' => 0,
            'MaxNumberOfInstallments' => intval($mesesParcelaCartao),              
          ];
          $Payment = new Payment($properties);
          
          // Instantiate customer's object.
          $properties = [
            'Identity' => $obParticipante->cliente->cpf,
            'FullName' => utf8_encode($obParticipante->cliente->nomeCompleto),
            'Email' => $obParticipante->cliente->email,
            'Phone' => $telefone,
          ];
          $Customer = new Customer($properties);
          
          // Instantiate options' object.
          $properties = [
            'AntifraudEnabled' => TRUE,
            'ReturnUrl' => str_replace('admin/','',$this->urlSite)."index.php",
          ];
          $Options = new Options($properties);
          
          // Instantiate order's object.
          $properties = [
            'OrderNumber' => $orderNumber,
            'SoftDescriptor' => 'RAINHATOUR',
            // Instantiate cart's object.
            'Cart' => new Cart(['Discount' => $Discount, 'Items' => $Items]),
            'Shipping' => $Shipping,
            'Payment' => $Payment,
            'Customer' => $Customer,
            'Options' => $Options,
          ];
          $Order = new Order($properties);
          $headers = array('Accept' => 'application/json','MerchantId'=>$this->cieloClientID,'Content-Type'=>'application/json; charset=utf-8');
          $query = Unirest\Request\Body::json($Order);
          $response = Unirest\Request::post($this->endpointCielo.'/orders', $headers, $query);

          if($response->code != 200 && $response->code != 201){
              if(isset($response->body)){
              throw new Exception($response->body->message);
              }else{
                  throw new Exception("Erro inesperado!");    
              }
          }

          if(isset($response->body->message)){
              throw new Exception($response->body->message);
          }

          $Transaction = $response->body;
        
          $this->checkoutUrl =  $Transaction->settings->checkoutUrl;
          $this->profile =  $Transaction->settings->profile;
          $this->version =  $Transaction->settings->version;
          $this->payment_installments = $mesesParcelaCartao;
          $this->save();
          if($this->conn->commit()){
          //enviar o boleto para verificacao por email:
          @$this->mail_html($this->REMETENTE,'','CART�O SISTEMA CIELO CRIADO','<a href="'. $Transaction->settings->checkoutUrl.'">'.$Transaction->settings->checkoutUrl);
          //envia  o email para o cliente
           //enviando email com dados da compra:
				 $html = "Peregrino ".$obParticipante->cliente->nomeCompleto.", foi gerado um novo link de pagamento via cart�o de cr�dito.<br/><br/>";
				 $html .= "Pacote de peregrina��o: ".$obParticipante->grupo->nomePacote.".<br/><br/>";
				 $html .= "Entre no link abaixo para realizar o pagamento.";
				 $html .= "<a href='".$this->checkoutUrl."'>Realizar Pagamento</a>";
				 $tplemail = new Template("templates/tpl_email_ecommerce.html");
         $tplemail->CONTEUDO = $html;
         if($this->mail_html($obParticipante->cliente->email,$this->REMETENTE, 'Obra de Maria DF - Reenvio de Pagamento '.str_pad($orderNumber,10,"0",STR_PAD_LEFT), $tplemail->showString())){
          return true;           
         }else{
          return false;
         }
				 return true;         
          }else{
            $this->conn->rollback();
            throw new Exception("Erro ao processar a solicita��o"); 
          }
         

        }
        catch(Exception $e) {
          $this->conn->rollback();
          throw $e;
        }
  }


  function UpdateByNotification($url,$idVenda,$logger){     
    $this->conn->autocommit(false);
    $this->conn->begin_transaction();
   try {
    if(!$this->confereConfiguracao()){
      throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
      exit();
    }
    $retorno = 'n�o foi executado nenhum procedimento';
    if(substr($idVenda,0,1) == "A"){
      $idCheckoutCielo = substr($idVenda,1);
      $this->getByIdWithParticipante($idCheckoutCielo);
      $headers = array('Accept' => 'application/json','MerchantId'=>$this->cieloClientID,'Content-Type'=>'application/json; charset=utf-8');
      //$query = Unirest\Request\Body::json($Order);
      $response = Unirest\Request::get($this->endpointCielo.'/orders/'.$this->cieloClientID.'/'.$idVenda, $headers);
      $logger->info($response->code);
      
      if($response->code != 200 && $response->code != 201){
          if(isset($response->body)){
            throw new Exception(isset($response->body->message) ? $response->body->message : $response->body);
          }else{
              throw new Exception("Erro inesperado!");    
          }
      }

      if(isset($response->body->message)){
          throw new Exception($response->body->message);
      }

      $order = $response->body;
      $this->payment_status = $order->payment_status;
      $this->payment_method_type = $order->payment_method_type;
      $this->payment_method_brand = $order->payment_method_brand;
      $this->payment_maskedcreditcard =$order->payment_maskedcreditcard;          
      $this->tid = $order->tid;
      $this->test_transaction = $order->test_transaction;
      $this->save();
      switch($order->payment_status){
      case '2':          
          $this->gerarPagamentoParticipante($order->amount);
          $retorno = 'pagamento gerado com suscesso';
      break;
      case '1':       
        $retorno =  'pagamento pendente';   
      break;
      case '3':          
        $retorno =  'pagamento foi negado!';
      break;
      case '4':
        $retorno =  'pagamento expirado';
      break;
      case '5':
        $retorno =  'pagamento cancelado';
      break;
      case '6':
        $retorno =  'pagamento n�o finalizado';
      break;
      case '7':
        $retorno =  'pagamento autorizado';
      break;
      case '8':
        $retorno =  'pagamento cancelado';
      break;
      default:
      $retorno =  'notificacao indeterminada';
      break;
      }


    }else{
        $vendas = $this->getByVendasId($idVenda);
        
        if(count($vendas) > 0){
          $this->getById($vendas[0]->id);
                $headers = array('Accept' => 'application/json','MerchantId'=>$this->cieloClientID,'Content-Type'=>'application/json; charset=utf-8');
                //$query = Unirest\Request\Body::json($Order);
                $response = Unirest\Request::get($this->endpointCielo.'/orders/'.$this->cieloClientID.'/'.$idVenda, $headers);
                $logger->info($response->code);
                if($response->code != 200 && $response->code != 201){
                    if(isset($response->body)){
                    throw new Exception($response->body->message);
                    }else{
                        throw new Exception("Erro inesperado!");    
                    }
                }

                if(isset($response->body->message)){
                    throw new Exception($response->body->message);
                }

                $order = $response->body;
              
     ;
      //tratar status do charge
              
              $this->payment_status = $order->payment_status;
              $this->payment_method_type = $order->payment_method_type;
              $this->payment_method_brand = $order->payment_method_brand;
              $this->payment_maskedcreditcard =$order->payment_maskedcreditcard;          
              $this->tid = $order->tid;
              $this->test_transaction = $order->test_transaction;
              $this->save();
      switch($order->payment_status){
          case '2':          
              $this->gerarPagamentos($order->amount);
              //enviando email com dados da compra:
              $html = "Parab�ns peregrino ".$this->venda->participante->nomeCompleto.", Seu Pagamento de R$ ".$this->money($this->amount,"atb")." foi aprovado!<br/><br/>";
              $html .= "O Valor ser� adicionado a sua reserva no roteiro de peregrina��o : ".$this->venda->participante->grupo->nomePacote.".<br/><br/>";
              $html .= "Para consultar sua reserva, clique no link abaixo ou entre em contato conosco!<br/>";
              $html .= "<a href='".$this->urlSite."/bilhete.php?charge_id=".$this->venda->id."'>Acessar minha reserva</a>";
              $tplemail = new Template("../templates/tpl_email_ecommerce.html");
              $tplemail->CONTEUDO = $html;
              $this->mail_html($this->venda->participante->email,$this->REMETENTE, 'Vendas Obra de Maria DF', $tplemail->showString());
              $retorno =  'pagamento gerado com suscesso';
          break;
          case '1':
            $retorno =  'pagamento pendente'; 
          break;
          case '3':
              //enviando email com dados da compra:
              $html = "Peregrino ".$this->venda->participante->nomeCompleto.", Seu Pagamento de R$ ".$this->money($this->amount,"atb")." foi recusado pela operadora do cart�o.<br/><br/>";
              $html .= "Entre em contato conosco para resolver o problema no roteiro de peregrina��o : ".$this->venda->participante->grupo->nomePacote.".<br/><br/>";
              $html .= "Para consultar sua reserva, clique no link abaixo ou entre em contato conosco!<br/>";
              $html .= "<a href='".$this->urlSite."/bilhete.php?charge_id=".$this->venda->id."'>Acessar minha reserva</a>";
              $tplemail = new Template("../templates/tpl_email_ecommerce.html");
              $tplemail->CONTEUDO = $html;
              $this->mail_html($this->venda->participante->email,$this->REMETENTE, 'Vendas Obra de Maria DF', $tplemail->showString());     
              $retorno =  'pagamento foi negado!';
          break;          
          case '4':
            $retorno =  'pagamento expirado';
          break;
          case '5':
            $retorno =  'pagamento cancelado';
          break;
          case '6':
            $retorno =  'pagamento n�o finalizado';
          break;
          case '7':
            $retorno =  'pagamento autorizado';
          break;
          case '8':
            $retorno =  'pagamento cancelado';
          break;
          default:
          $retorno =  'notificacao indeterminada';
          break;
      }
   
      }else{
        $retorno =  "n�o foi executado nenhum procedimento";
      }
    }
    $this->conn->commit();
    return $retorno;
   
   } catch (Exception $e) {
      $this->conn->rollback();
      throw $e;
  }
   }

   private function gerarPagamentoParticipante($valorPago){
        $pag = new Pagamento();  
        if(!$pag->getRow(array("cielo"=>"=".$this->id))){
        $valor = $this->convertvalorGerenciaNet($valorPago,'gtd');
        $part = new Participante();
        $part->getById($this->participante->id);
        $oTipoP = new TipoPagamento();
        $oFin = new FinalidadePagamento();  
                
        $om = new Moeda();
        $oTipoP->id = $oTipoP->CIELO();
        $oFin->id = 1;
        $om->id = $om->REAL();
        $pag->dataPagamento = date("Y-m-d");
        $pag->valorPagamento = $this->money($valor,"bta");
        $pag->obs = 'pagamento autom�tico vindo do cielo';
        $pag->abatimentoAutomatico =1;
        $pag->moeda = $om;
        $pag->participante = $this->participante;
        $pag->tipo = $oTipoP;
        $pag->finalidade = $oFin;
        $pag->cancelado = 0;
        $pag->devolucao = 0;
        $pag->valorParcela = 0;
        $pag->cotacaoMoedaReal=0;
        $pag->cotacaoReal = $this->participante->grupo->cotacaoParcelado;
        $pag->parcela = $this->payment_installments;
        $pag->site = 0;
        $pag->pago = 1;
        $pag->cielo = $this;
        $idBandeira = $this->payment_method_brand;
        if($idBandeira == 4){
          $idBandeira = 6;
        }else if($idBandeira == 5){
          $idBandeira = 8;
        }else if($idBandeira == 6){
          $idBandeira = 4;
        }else if($idBandeira == 7){
          $idBandeira = 10; 
        }else if($idBandeira == 8){
          $idBandeira = 9; 
        }else if($idBandeira == 9){
          $idBandeira = 5;  
        }

        $pag->bandeira =new BandeiraCartao($idBandeira);

        $pag->save();
        $oAbat = new Abatimento();	
        $oG = new Grupo();
        $oG->getById($this->participante->grupo->id);
        if($oG->moeda->id == $om->DOLLAR()){
            $oAbat->valor = $pag->CALCULA_DOLLAR();
        }else{
            $oAbat->valor = $pag->CALCULA_REAL();
        }	
        $oAbat->participante = $part;
        $oAbat->pagamento = $pag;
        $oAbat->save();
        
        $part->atualiza_status();
        }

   }


   private function gerarPagamentos($valorPago){
    $qtd = 1;
    if($this->venda->acompanhante1 != null)
        $qtd++;
    if($this->venda->acompanhante2 != null)
        $qtd++;
    if($this->venda->acompanhante3 != null)
        $qtd++;
    if($this->venda->acompanhante4 != null)
        $qtd++;
    
    
    
    //converte para decimal o valor
    $valor = $this->convertvalorGerenciaNet($valorPago,'gtd')/$qtd;
    
    

    for($i=1;$i<=$qtd;$i++){
      $pag = new Pagamento();  
      if(!$pag->getRow(array("cielo"=>"=".$this->id))){
        $part = new Participante();
        $oTipoP = new TipoPagamento();
        $oFin = new FinalidadePagamento();  
               
        $om = new Moeda();
        $oTipoP->id = $oTipoP->CIELO();
        $oFin->id = 1;
        $om->id = $om->REAL();
        if($i==1){
            $part->getById($this->venda->participante->id);
        }else if($i == 2){
            $part->getById($this->venda->acompanhante1);
        }else if($i == 3){
            $part->getById($this->venda->acompanhante2);
        }else if($i == 4){
            $part->getById($this->venda->acompanhante3);
        }else if($i == 5){
            $part->getById($this->venda->acompanhante4);
        }
        
        $pag->dataPagamento = date("Y-m-d");
        $pag->valorPagamento = $this->money($valor,"bta");
        $pag->obs = 'pagamento autom�tico vindo do cielo';
        $pag->abatimentoAutomatico =1;
        $pag->moeda = $om;
        $pag->participante = $this->venda->participante;
        $pag->tipo = $oTipoP;
        $pag->finalidade = $oFin;
        $pag->cancelado = 0;
        $pag->devolucao = 0;
        $pag->valorParcela = 0;
        $pag->cotacaoMoedaReal=0;
        $pag->cotacaoReal = $this->venda->cotacao;
        $pag->parcela = $this->payment_installments;
        $pag->site = 1;
        $pag->pago = 1;
        $pag->cielo = $this;
        $idBandeira = $this->payment_method_brand;
        if($idBandeira == 4){
          $idBandeira = 6;
        }else if($idBandeira == 5){
          $idBandeira = 8;
        }else if($idBandeira == 6){
          $idBandeira = 4;
        }else if($idBandeira == 7){
          $idBandeira = 10; 
        }else if($idBandeira == 8){
          $idBandeira = 9; 
        }else if($idBandeira == 9){
          $idBandeira = 5;  
        }

        $pag->bandeira =new BandeiraCartao($idBandeira);

        $pag->save();
        $oAbat = new Abatimento();	
        $oG = new Grupo();
    $oG->getById($this->venda->participante->grupo->id);
        if($oG->moeda->id == $om->DOLLAR()){
            $oAbat->valor = $pag->CALCULA_DOLLAR();
        }else{
            $oAbat->valor = $pag->CALCULA_REAL();
        }	
        $oAbat->participante = $part;
        $oAbat->pagamento = $pag;
        $oAbat->save();
        
      $part->atualiza_status();
    }
  }
    
    
}
}