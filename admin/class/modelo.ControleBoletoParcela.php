<?php
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
class ControleBoletoParcela extends Persistencia {
    var $valorReal;
    var $valorMoeda;
    var $cotacao;
    var $controleBoleto = null;
    var $isGerado;
    var $dataVencimento;
    var $numero;
    var $gnStatus;
    var $gnChargeId;
    var $gnNumeroFebraban;
    var $gnUrlBoleto;
    var $dataPagamento;

    public function getBoletosVencidosPorGrupo($idGrupo){
        $hoje = DateTime::createFromFormat("Y-m-d",date("Y-m-d"));  
        $sql = "select * from ag_controle_boleto_parcela p 
        inner join ag_controle_boleto b on b.id = p.id_controle_boleto
        inner join ag_participante pa on pa.id = b.id_participante 
        inner join ag_grupo g on g.id = pa.grupo where data_vencimento < '".$hoje->format("Y-m-d")."' and p.gn_status != 'paid' and g.id = $idGrupo order by data_vencimento asc" ;      
        return $this->getSQL($sql);
    }

    public function getBoletosLiquidadosPorGrupo($idGrupo){
        $sql = "select * from ag_controle_boleto_parcela p 
        inner join ag_controle_boleto b on b.id = p.id_controle_boleto
        inner join ag_participante pa on pa.id = b.id_participante 
        inner join ag_grupo g on g.id = pa.grupo where p.gn_status = 'paid' and g.id = $idGrupo  order by data_vencimento asc" ;      
        return $this->getSQL($sql);
    }

    public function getBoletosAVencerPorGrupo($idGrupo){
        $hoje = DateTime::createFromFormat("Y-m-d",date("Y-m-d"));  
        $sql = "select * from ag_controle_boleto_parcela p 
        inner join ag_controle_boleto b on b.id = p.id_controle_boleto
        inner join ag_participante pa on pa.id = b.id_participante 
        inner join ag_grupo g on g.id = pa.grupo where data_vencimento >= '".$hoje->format("Y-m-d")."' and p.gn_status != 'paid' and g.id = $idGrupo order by data_vencimento asc" ;     
        return $this->getSQL($sql);
    }

    public function getBoletosAvencer($dias){
        $hoje = DateTime::createFromFormat("Y-m-d",date("Y-m-d"));
        $hoje->add(new DateInterval("P".$dias."D"));
        return $this->getRows(0,99999,array(),array("isGerado"=>"=0","dataVencimento"=>"<='".$hoje->format("Y-m-d")."'"));
    }

    public function getTotalValorFaturadoByMoeda($moeda){
        $sql = "select sum(p.valor_moeda) as total from ag_controle_boleto_parcela p 
        inner join ag_controle_boleto b on b.id = p.id_controle_boleto
        inner join ag_participante pa on pa.id = b.id_participante 
        inner join ag_grupo g on g.id = pa.grupo where g.idMoeda = $moeda and p.gn_status != 'paid'";
        $rs = $this->DAO_ExecutarQuery($sql);
        $array = $this->DAO_GerarArray($rs);
        return $array['total'];
    }

    public function getTotalValorRecebidoMes(){
        $sql = "select sum(p.valor_real) as total from ag_controle_boleto_parcela p 
        inner join ag_controle_boleto b on b.id = p.id_controle_boleto
        inner join ag_participante pa on pa.id = b.id_participante 
        inner join ag_grupo g on g.id = pa.grupo where p.gn_status = 'paid' and p.data_pagamento between '".date("Y-m")."-01"."' and '".date("Y-m-t")."'";
        $rs = $this->DAO_ExecutarQuery($sql);
        $array = $this->DAO_GerarArray($rs);
        return $array['total'];
    }

    public function getTotalVencidos(){
        $sql = "select count(p.id) as total from ag_controle_boleto_parcela p 
        inner join ag_controle_boleto b on b.id = p.id_controle_boleto
        inner join ag_participante pa on pa.id = b.id_participante 
        inner join ag_grupo g on g.id = pa.grupo where p.gn_status != 'paid' and p.data_vencimento <= '".date("Y-m-d")."'";
        $rs = $this->DAO_ExecutarQuery($sql);
        $array = $this->DAO_GerarArray($rs);
        return $array['total'];
    }

    public function getTotalBoletos(){
        $sql = "select count(p.id) as total from ag_controle_boleto_parcela p";
        $rs = $this->DAO_ExecutarQuery($sql);
        $array = $this->DAO_GerarArray($rs);
        return $array['total'];
    }

    public function getByChargeId($chargeId){
        return $this->getRow(array("gnChargeId"=>"=".$chargeId));
    }

    public function gerar($req){
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();
        try{
            if(!$this->confereConfiguracao()){
                throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
                exit();
              }
        $this->getById($req['idParcela']);
        $this->isGerado = true;
        $this->cotacao = $this->money($req['cotacao'],'bta');
        $this->valorReal =$this->money($this->valorMoeda*$this->cotacao,'bta');
        $this->save();
        $this->gn_geraBoleto();

        //enviar email para o cliente
        $html = "Peregrino ".$this->controleBoleto->participante->cliente->nomeCompleto.", Seu Boleto de R$ ".$this->money($this->valorReal,"atb")." referente a parcela ".$this->numero."<br/>";
        $html .= "do pacote de peregrina��o ".$this->controleBoleto->participante ->grupo->nomePacote." foi gerado!<br/>";
        $html .= "Clique no link abaixo para imprimir <br/> <a  href='".$this->gnUrlBoleto."'>Abrir Boleto</a><br/>";
        $html .= "Obrigado!";
        $tplemail = new Template($this->URI."templates/tpl_email_ecommerce.html");
        $tplemail->CONTEUDO = $html;
        $this->mail_html($this->controleBoleto->participante->cliente->email,$this->REMETENTE, 'Obra de Maria DF', $tplemail->showString());
        return $this->conn->commit();



        }catch(Exception $e){
            $this->conn->rollback();
            $_SESSION['tupi.mensagem'] = 'danger;'.$e->getMessage();
            return false;
        }
        
    }

    public function gn_geraBoleto(){
        
        $options = [
        'client_id' => $this->clientIdGN,
        'client_secret' => $this->clientSecretGN,
        'sandbox' => $this->debug // altere conforme o ambiente (true = desenvolvimento e false = producao)
        ];
        
        $itensGN = array();
        
            $itemadd = [
                'name' => 'Parcela '.$this->numero."/".$this->controleBoleto->numeroParcelas." - ".utf8_encode($this->controleBoleto->participante->grupo->nomePacote), // nome do item, produto ou servi�o
                'amount' => 1, // quantidade
                'value' => intval(str_replace(".","",str_replace(",","",$this->money($this->valorReal,'atb'))))
            ];
            $itensGN[0]=$itemadd;
        
        $metadata = ['notification_url'=>$this->urlScripts."gn_boletos.php"];
        $body  =  [
            'items' => $itensGN,
            'metadata' => $metadata
        ];
        
        
        try {
            if(!$this->confereConfiguracao()){
                throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
                exit();
              }
            //registra o pagamento
            $api = new Gerencianet($options);
            $charge = $api->createCharge([], $body);
            
        //Array ( [code] => 200 [data] => Array ( [charge_id] => 448859 [status] => new [total] => 2000 [custom_id] => [created_at] => 2018-07-26 11:33:30 ) ) 

            $this->gnStatus = $charge['data']['status'];
            $this->gnChargeId = $charge['data']['charge_id'];
            if(strlen($this->controleBoleto->participante->cliente->celular) > 0 ){
                $telefone = $this->controleBoleto->participante->cliente->celular;
            }else if(strlen($this->controleBoleto->participante->cliente->telefoneResidencial) > 0){
            $telefone = $this->controleBoleto->participante->cliente->telefoneResidencial;
            }else{
                $telefone =$this->controleBoleto->participante->cliente->telefoneComercial;
            } 
            
            
            if(strlen($telefone) == 0){
            throw new Exception('Telefones invalidos');
            }
                //seta boleto
                // $charge_id refere-se ao ID da transa��o gerada anteriormente
                    $params = [
                    'id' => intval($this->gnChargeId)
                    ];
                    //trata o telefone
                    $phone_number = $this->limpaDigitos($telefone);
                    
                    if(strlen($phone_number) == 11){
                        $phone_number = substr($phone_number,0,2).'9'.substr($phone_number,3,8);
                    }else if(strlen($phone_number) > 11){
                        $phone_number = substr($phone_number,2,2).'9'.substr($phone_number,4,8);
                    }else if (strlen($phone_number) < 10){
                        $phone_number = str_pad($phone_number,10,"0",STR_PAD_LEFT);
                    } 
                    
                    
                    $customer = [
                            'name' => utf8_encode($this->controleBoleto->participante->cliente->nomeCompleto), // nome do cliente
                            'cpf' => $this->controleBoleto->participante->cliente->cpf , // cpf valido do cliente
                            'phone_number' => $phone_number
                        ];
                        
                    
                    

                    $bankingBillet = [
                    'expire_at' => $this->dataVencimento, // data de vencimento do boleto (formato: YYYY-MM-DD)
                    'customer' => $customer
                    ];
                    
                    $payment = [
                    'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
                    ];
                    
                    $body = [
                    'payment' => $payment
                    ];


        
                    $charge = $api->payCharge($params, $body);
                    //Array ( [code] => 200 [data] => Array ( [barcode] => 00000.00000 00000.000000 00000.000000 0 00000000000000 [link] => https://visualizacaosandbox.gerencianet.com.br/emissao/43219_1_CACA2/A4XB-43219-305988-MAXI8 [expire_at] => 2018-07-26 [charge_id] => 449048 [status] => waiting [total] => 6000 [payment] => banking_billet ) ) 
                    $this->gnStatus = $charge['data']['status'];
                    $this->gnNumeroFebraban = $charge['data']['barcode'];
                    $this->gnUrlBoleto = $charge['data']['link'];
                    $this->save();

                    //enviar o boleto para verificacao por email:
                    @$this->mail_html($this->REMETENTE,'','BOLETO SISTEMA GERENCIANET CRIADO','<a href="'.$charge['data']['link'].'">'.$charge['data']['link']);
                   						
                    
        } catch (GerencianetException $e) {
            
            throw new Exception($e->getMessage());
        
        }
    }
    function getOptions(){
        $clientId = $this->clientIdGN; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
        $clientSecret = $this->clientSecretGN; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
         
        $options = [
          'client_id' => $clientId,
          'client_secret' => $clientSecret,
          'sandbox' => $this->debug // altere conforme o ambiente (true = desenvolvimento e false = producao)
        ];
        return $options;
    }

    function UpdateByNotification($logger){
        @$logger->info('entrando no update notification-'.date("H:i:s"));
        if(isset($_POST["notification"])){
            @$logger->info('NOTIFICATION-'.date("H:i:s")."-".$_POST["notification"]);
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();

        $token = $_POST["notification"];
        
        $params = [
        'token' => $token
        ];
 
    try {
        if(!$this->confereConfiguracao()){
            throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
            exit();
          }
        $api = new Gerencianet($this->getOptions());
        $chargeNotification = $api->getNotification($params, []);
      $i = count($chargeNotification["data"]);
        // Pega o �ltimo Object chargeStatus
        $ultimoStatus = $chargeNotification["data"][$i-1];
        // Acessando o array Status
        $status = $ultimoStatus["status"];
        // Obtendo o ID da transa��o    
        $charge_id = $ultimoStatus["identifiers"]["charge_id"];
        
        // Obtendo a String do status atual
    $statusAtual = $status["current"];
    @$logger->info('STATUSATUAL-'.date("H:i:s")."-".$statusAtual);
    @$logger->info('CHARGE_ID-'.date("H:i:s")."-".$charge_id);
    //atualiza o pagamento
    //atualiza o pagamento
    $this->getByChargeId($charge_id);
    $this->gnStatus =$statusAtual;
   // $this->token = $token;
    $this->save();
    //tratar status do charge
    $retorno = "n�o foi executado nenhum procedimento";
    switch($statusAtual){
        case 'paid':
            $this->gerarPagamento();
            $html = "Peregrino ".$this->controleBoleto->participante->cliente->nomeCompleto.", Seu Pagamento de R$ ".$this->money($this->valorReal,"atb")." referente a parcela ".$this->numero."<br/>";
            $html .= "do pacote de peregrina��o ".$this->controleBoleto->participante ->grupo->nomePacote." foi confirmado!<br/>";
            $html .= "Obrigado!";
            $tplemail = new Template($this->URI."templates/tpl_email_ecommerce.html");
            $tplemail->CONTEUDO = $html;
          $this->mail_html($this->controleBoleto->participante->cliente->email,$this->REMETENTE, 'Obra de Maria DF', $tplemail->showString());
          $this->mail_html($this->DESTINATARIO,$this->REMETENTE, 'Obra de Maria DF - C�pia email confirma��o de pagamento', $tplemail->showString());
          $retorno = 'pagamento gerado com suscesso';
        break;
        case 'canceled':
            $retorno = 'cancelado';    
        break;
        case 'contested':
            $retorno = 'contestado';    
        break;
        case 'settled':
            $retorno = 'mudanca';    
        break;
    }
    


    $this->conn->commit();
    
    return $retorno;
    
   
    //print_r($chargeNotification);
        } catch (GerencianetException $e) {
            $this->conn->rollback();
            throw $e;
        } catch (Exception $e) {
            $this->conn->rollback();
            throw $e;
        }
    }else{
        return "n�o foi executado nenhum procedimento";
    }
}

    private function gerarPagamento(){      
        
      
            $part = new Participante();
            $oTipoP = new TipoPagamento();
            $oFin = new FinalidadePagamento();  
            $pag = new Pagamento();          
            $om = new Moeda();
            $oTipoP->id = $oTipoP->GERENCIA_NET();
            $oFin->id = 1;
            $om->id = $om->REAL();
            $part->getById($this->controleBoleto->participante->id);          
            
            $pag->dataPagamento = date("Y-m-d");
            $pag->valorPagamento = $this->valorReal;
            $pag->obs = 'pagamento autom�tico vindo da gerencianet';
            $pag->abatimentoAutomatico =1;
            $pag->moeda = $om;
            $pag->participante = $part;
	        $pag->tipo = $oTipoP;
            $pag->finalidade = $oFin;
            $pag->cancelado = 0;
	        $pag->devolucao = 0;
            $pag->valorParcela = 0;
            $pag->cotacaoMoedaReal=0;
		    $pag->cotacaoReal = $this->cotacao;
            $pag->parcela = 1;
            $pag->site = 0;
	        $pag->pago = 1;
            $pag->save();
            $oAbat = new Abatimento();	
            $oG = new Grupo();
		    $oG->getById($this->controleBoleto->participante->grupo->id);
            if($oG->moeda->id == $om->DOLLAR()){
                $oAbat->valor = $pag->CALCULA_DOLLAR();
            }else{
                $oAbat->valor = $pag->CALCULA_REAL();
            }	
            $oAbat->participante = $part;
            $oAbat->pagamento = $pag;
            $oAbat->save();
            
	        $part->atualiza_status();
        
        
        
    }

    public function updateParcela($req){
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();
        try{
        $this->getById($req['idParcela']);
        $this->dataVencimento = $this->convdata($req['dataVencimento'],'ntm');
        $this->valorMoeda = $this->money($req['valor'],"bta");
        $this->save();        
        return $this->conn->commit();
        }catch(Exception $e){
            $this->conn->rollback();
            $_SESSION['tupi.mensagem'] = 'danger;'.$e->getMessage();
            return false;
        }
        
    }


}