<?php 
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;


class GerenciaNetCheckOut extends Persistencia {
    var $id = NULL;
	var $charge_id;
	var $status;	
	var $message = NULL;
    var $participante = NULL;
    var  $venda = NULL;
    var $total;
    var $created_at;
    var $payment_method;
    var $update_at;
    var $payment_url;
    var $token;
    var $participanteDono = NULL;
    var $cotacao = 0;
    
    public function getByIdWithParticipante($id){
        return $this->getRow(array("id"=>"=".$id,"venda"=>"is null"));
      }
  
      public function getByParticipanteId($participanteId){
        return $this->getRows(0,99,array(),array("participanteDono"=>"=".$participanteId));
      }

public function getByChargeId($chargeId){
    return $this->getRow(array("charge_id"=>"=".$chargeId));
}

public function getByVendasId($vendaId){
    return $this->getRows(0,4,array(),array("venda"=>"=".$vendaId,"participanteDono"=>"is null"));
}

public function getByVendasNaoPagasId($vendaId){
    return $this->getRows(0,4,array(),array("venda"=>"=".$vendaId,"status"=>"!='paid'"));
}

function createLinkPagamento($charge_id,$mensagem,$tipoPagamento,$dataVencimento=""){

    
// $charge_id refere-se ao ID da transa��o gerada anteriormente
$params = [
  'id' => $charge_id
];
if($dataVencimento==""){
$date = new DateTime(date("Y-m-d"));
$date->add(new DateInterval('P3D'));
}else{
$date = new DateTime($this->convdata($dataVencimento,"ntm"));    
}

$body = [
  'message' => utf8_encode($mensagem), // mensagem para o pagador com at� 80 caracteres
  'expire_at' => $date->format('Y-m-d') , // data de vencimento da tela de pagamento e do pr�prio boleto
  'request_delivery_address' => false, // solicitar endere�o de entrega do comprador?
  'payment_method' => $tipoPagamento // formas de pagamento dispon�veis
];

try {
    if(!$this->confereConfiguracao()){
        throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
        exit();
      }
    $api = new Gerencianet($this->getOptions());
  $response = $api->linkCharge($params, $body);
  $this->payment_method = $response['data']['payment_method'];
  $this->payment_url = $response['data']['payment_url'];
  $this->status = $response['data']['status'];
  $this->update_at = date("Y-m-d H:i:s");
  $this->save();
  return $response;
        } catch (GerencianetException $e) {
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }


}

    function createCharge($obParticipante,$obGrupo,$obVenda,$valor){
       if($obVenda->opcional)
        $nomeItem = $obGrupo->nomePacote."+".$obGrupo->nomePacoteOpcional;
        else
        $nomeItem = $obGrupo->nomePacote;
        $item_1 = [
            'name' => utf8_encode($nomeItem), // nome do item, produto ou servi�o
            'amount' => intval($obVenda->quantidade), // quantidade
            'value' => intval(str_replace(".","",str_replace(",","",$this->money($valor,"atb")))) // valor (1000 = R$ 10,00) (Obs: � poss�vel a cria��o de itens com valores negativos. Por�m, o valor total da fatura deve ser superior ao valor m�nimo para gera��o de transa��es.)
        ];
         
        $items =  [
            $item_1
        ];
        $metadata = [
            "custom_id"=> strval($obVenda->id),
            "notification_url"=>$this->urlScripts."chargegn.php"
        ];
        
        // Exemplo para receber notifica��es da altera��o do status da transa��o.
        // $metadata = ['notification_url'=>'sua_url_de_notificacao_.com.br']
        // Outros detalhes em: https://dev.gerencianet.com.br/docs/notificacoes
        
        // Como enviar seu $body com o $metadata
        // $body  =  [
        //    'items' => $items,
        //    'metadata' => $metadata
        // ];
        
        $body  =  [
            'items' => $items,
            'metadata' => $metadata
        ];
        
        try {
            if(!$this->confereConfiguracao()){
                throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
                exit();
              }
            $api = new Gerencianet($this->getOptions());
            $charge = $api->createCharge([], $body);
            
            $this->venda = $obVenda;
            $this->participante = $obParticipante;
            $this->charge_id = $charge['data']['charge_id'];	
            $this->status  = "new";
            $this->message = "Cria��o do ChargeId";
            $this->total = $this->money($valor,"bta");           
            $this->created_at = date("Y-m-d H:i:s");
            $this->update_at = date("Y-m-d H:i:s");
            $this->save();
            return $charge;
        } catch (GerencianetException $e) {
           
            throw $e;
        } catch (Exception $e) {
            
            throw $e;
        }
    }

    function createChargeParticipante($obParticipante,$valor,$cotacao,$vencimento=""){
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();
            $this->status  = "new";
            $this->message = "Cria��o do ChargeId";
            $this->total = $this->money($valor,"bta");           
            $this->created_at = date("Y-m-d H:i:s");
            $this->update_at = date("Y-m-d H:i:s");
            $this->participanteDono = $obParticipante;
            $this->participante = $obParticipante;
            $this->cotacao = $this->money($cotacao,"bta"); 
            $orderNumber = "A".strval($this->save());
          
        $nomeItem = $obParticipante->grupo->nomePacote;
        $item_1 = [
            'name' => utf8_encode($nomeItem), // nome do item, produto ou servi�o
            'amount' => 1, // quantidade
            'value' => intval(str_replace(".","",str_replace(",","",$this->money($valor,"bta")))) // valor (1000 = R$ 10,00) (Obs: � poss�vel a cria��o de itens com valores negativos. Por�m, o valor total da fatura deve ser superior ao valor m�nimo para gera��o de transa��es.)
        ];
         
        $items =  [
            $item_1
        ];
        $metadata = [
            "custom_id"=> strval($orderNumber),
            "notification_url"=>$this->urlScripts."chargegn.php"
        ];
        
        
        $body  =  [
            'items' => $items,
            'metadata' => $metadata
        ];
        
        try {
            if(!$this->confereConfiguracao()){
                throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
                exit();
              }
            $api = new Gerencianet($this->getOptions());
            $charge = $api->createCharge([], $body);          
            $this->charge_id = $charge['data']['charge_id'];	
            $this->status = $charge['data']['status'];
            $this->update_at = date("Y-m-d H:i:s");
            $params = [
                'id' => intval($this->charge_id)
                ];
                //trata o telefone
                $phone_number = $this->limpaDigitos($obParticipante->cliente->celular);
                
                if(strlen($phone_number) == 11){
                    $phone_number = substr($phone_number,0,2).'9'.substr($phone_number,3,8);
                }else if(strlen($phone_number) > 11){
                    $phone_number = substr($phone_number,2,2).'9'.substr($phone_number,4,8);
                }else if (strlen($phone_number) < 10){
                    $phone_number = str_pad($phone_number,10,"0",STR_PAD_LEFT);
                } 
                
                
                $customer = [
                        'name' => utf8_encode($obParticipante->cliente->nomeCompleto), // nome do cliente
                        'cpf' => $obParticipante->cliente->cpf , // cpf valido do cliente
                        'phone_number' => $phone_number
                    ];
                    
                
                if($vencimento==""){
                    $date = new DateTime(date("Y-m-d"));
                    $date->add(new DateInterval('P3D'));
                }else{
                    $date = new DateTime($this->convdata($vencimento,"ntm"));    
                }
                $bankingBillet = [
                'expire_at' => $date->format('Y-m-d'), // data de vencimento do boleto (formato: YYYY-MM-DD)
                'customer' => $customer
                ];                
                $payment = [
                'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
                ];                
                $body = [
                'payment' => $payment
                ];


    
                $charge = $api->payCharge($params, $body);
                //Array ( [code] => 200 [data] => Array ( [barcode] => 00000.00000 00000.000000 00000.000000 0 00000000000000 [link] => https://visualizacaosandbox.gerencianet.com.br/emissao/43219_1_CACA2/A4XB-43219-305988-MAXI8 [expire_at] => 2018-07-26 [charge_id] => 449048 [status] => waiting [total] => 6000 [payment] => banking_billet ) ) 
                $this->status = $charge['data']['status'];
                $this->payment_method = 'banking_billet';
                $this->payment_url = $charge['data']['link'];
                $this->update_at = date("Y-m-d H:i:s");
                $this->save();
                //enviar o boleto para verificacao por email:
                @$this->mail_html($this->REMETENTE,'','BOLETO INTERNET GERENCIANET CRIADO','<a href="'.$charge['data']['link'].'">'.$charge['data']['link']);

            //$this->createLinkPagamento($this->charge_id,'','banking_billet',$vencimento);
            if($this->conn->commit()){
                    $html = "Peregrino ".$obParticipante->cliente->nomeCompleto.", foi gerado um novo link de pagamento via boleto banc�rio.<br/><br/>";
                    $html .= "Pacote de peregrina��o: ".$obParticipante->grupo->nomePacote.".<br/><br/>";
                    $html .= "Entre no link abaixo para realizar o pagamento.";
                    $html .= "<a href='".$this->payment_url."'>Realizar Pagamento</a>";
                    $tplemail = new Template("templates/tpl_email_ecommerce.html");
                    $tplemail->CONTEUDO = $html;
                    if($this->mail_html($obParticipante->cliente->email,$this->REMETENTE, 'Obra de Maria DF - Reenvio de Pagamento '.str_pad($orderNumber,10,"0",STR_PAD_LEFT), $tplemail->showString())){
                        return true;           
                    }else{
                        return false;
                    }
                    return true;         
                }else{
                    $this->conn->rollback();
                    throw new Exception("Erro ao processar a solicita��o"); 
                }
            
             } catch (GerencianetException $e) {
                error_log(print_r($e));
                $this->conn->rollback();
            throw $e;
        } catch (Exception $e) {
            $this->conn->rollback();
            throw $e;
        }
       


        if($this->conn->commit()){
            //envia  o email para o cliente
             //enviando email com dados da compra:
                   $html = "Peregrino ".$obParticipante->cliente->nomeCompleto.", foi gerado um novo link de pagamento via cart�o de cr�dito.<br/><br/>";
                   $html .= "Pacote de peregrina��o: ".$obParticipante->grupo->nomePacote.".<br/><br/>";
                   $html .= "Entre no link abaixo para realizar o pagamento.";
                   $html .= "<a href='".$this->checkoutUrl."'>Realizar Pagamento</a>";
                   $tplemail = new Template("templates/tpl_email_ecommerce.html");
           $tplemail->CONTEUDO = $html;
           if($this->mail_html($obParticipante->cliente->email,$this->REMETENTE, 'Obra de Maria DF - Reenvio de Pagamento '.str_pad($orderNumber,10,"0",STR_PAD_LEFT), $tplemail->showString())){
            return true;           
           }else{
            return false;
           }
                   return true;         
            }else{
              throw new Exception("Erro ao processar a solicita��o"); 
            }
    }

    function getOptions(){
        $clientId = $this->clientIdGN; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
        $clientSecret = $this->clientSecretGN; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
         
        $options = [
          'client_id' => $clientId,
          'client_secret' => $clientSecret,
          'sandbox' => $this->debug // altere conforme o ambiente (true = desenvolvimento e false = producao)
        ];
        return $options;
    }

    function UpdateByNotification(){

    if(isset($_POST["notification"])){

        $token = $_POST["notification"];
        
        $params = [
        'token' => $token
        ];
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();
    try {
        if(!$this->confereConfiguracao()){
            throw new Exception("Ocorreu um problema em nossos servidores, tente novamente mais tarde");
            exit();
          }
        $api = new Gerencianet($this->getOptions());
        $chargeNotification = $api->getNotification($params, []);
      $i = count($chargeNotification["data"]);
        // Pega o �ltimo Object chargeStatus
        $ultimoStatus = $chargeNotification["data"][$i-1];
        // Acessando o array Status
        $status = $ultimoStatus["status"];
        // Obtendo o ID da transa��o    
        $charge_id = $ultimoStatus["identifiers"]["charge_id"];
        
        // Obtendo a String do status atual
    $statusAtual = $status["current"];
    
    //atualiza o pagamento
    $this->getByChargeId($charge_id);
    $this->status =$statusAtual;
    $this->token = $token;
    $this->save();
    //tratar status do charge
    $retorno = "n�o foi executado nenhum procedimento";
    switch($statusAtual){
        case 'paid':
            $this->gerarPagamentos($ultimoStatus["value"]);
            $html = "Parab�ns peregrino ".$this->participante->cliente->nomeCompleto.", Seu Pagamento de R$ ".$this->money($this->total,"atb")." foi aprovado!<br/><br/>";
          $html .= "O Valor ser� adicionado a sua reserva no roteiro de peregrina��o : ".$this->participante->grupo->nomePacote.".<br/><br/>";
          if($this->venda != null){
          $html .= "Para consultar sua reserva, clique no link abaixo ou entre em contato conosco!<br/>";
          $html .= "<a href='".$this->urlSite."/bilhete.php?charge_id=".$this->venda->id."'>Acessar minha reserva</a>";
          }
          $tplemail = new Template("../templates/tpl_email_ecommerce.html");
          $tplemail->CONTEUDO = $html;
          $this->mail_html($this->participante->cliente->email,$this->REMETENTE, 'Vendas Obra de Maria DF', $tplemail->showString());
          $this->mail_html($this->DESTINATARIO,$this->REMETENTE, 'Obra de Maria DF - C�pia email confirma��o de pagamento', $tplemail->showString());
          $retorno = 'pagamento gerado com suscesso';
          break;
          case 'canceled':
              $retorno = 'cancelado';    
          break;
          case 'contested':
              $retorno = 'contestado';    
          break;
          case 'settled':
              $retorno = 'mudanca';    
          break;
    }
    


    
    $this->conn->commit();
    
    return $retorno;
    
   
    //print_r($chargeNotification);
} catch (GerencianetException $e) {
    $this->conn->rollback();
    throw $e;
} catch (Exception $e) {
    $this->conn->rollback();
    throw $e;
}
    }else{
        return "n�o foi executado nenhum procedimento";
    }
    }

    private function gerarPagamentos($valorPago){
        $qtd = 1;
        if($this->venda != null){
        if($this->venda->acompanhante1 != null)
            $qtd++;
        if($this->venda->acompanhante2 != null)
            $qtd++;
        if($this->venda->acompanhante3 != null)
            $qtd++;
        if($this->venda->acompanhante4 != null)
            $qtd++;
        }
        
        
        //converte para decimal o valor
        $valor = $this->convertvalorGerenciaNet($valorPago,'gtd')/$qtd;
        
        

        for($i=1;$i<=$qtd;$i++){
            $part = new Participante();
            $oTipoP = new TipoPagamento();
            $oFin = new FinalidadePagamento();  
            $pag = new Pagamento();          
            $om = new Moeda();
            $oTipoP->id = $oTipoP->GERENCIA_NET();
            $oFin->id = 1;
            $om->id = $om->REAL();
            if($i==1){
                $part->getById($this->participante->id);
            }else if($i == 2){
                $part->getById($this->venda->acompanhante1);
            }else if($i == 3){
                $part->getById($this->venda->acompanhante2);
            }else if($i == 4){
                $part->getById($this->venda->acompanhante3);
            }else if($i == 5){
                $part->getById($this->venda->acompanhante4);
            }
            
            $pag->dataPagamento = date("Y-m-d");
            $pag->valorPagamento = $this->money($valor,"bta");
            $pag->obs = 'pagamento autom�tico vindo da gerencianet';
            $pag->abatimentoAutomatico =1;
            $pag->moeda = $om;
            $pag->participante = $this->participante;
	        $pag->tipo = $oTipoP;
            $pag->finalidade = $oFin;
            $pag->cancelado = 0;
	        $pag->devolucao = 0;
            $pag->valorParcela = 0;
            $pag->cotacaoMoedaReal=0;
		    $pag->cotacaoReal = $this->venda != null ? $this->venda->cotacao : $this->cotacao;
            $pag->parcela = 1;
            $pag->site = 1;
	        $pag->pago = 1;
            $pag->save();
            $oAbat = new Abatimento();	
            $oG = new Grupo();
		    $oG->getById($this->participante->grupo->id);
            if($oG->moeda->id == $om->DOLLAR()){
                $oAbat->valor = $pag->CALCULA_DOLLAR();
            }else{
                $oAbat->valor = $pag->CALCULA_REAL();
            }	
            $oAbat->participante = $part;
            $oAbat->pagamento = $pag;
            $oAbat->save();
            
	        $part->atualiza_status();
        }
        
        
    }

    

    function getMetodo(){
        return $this->payment_method == 'banking_billet' ? 'Boleto' : "Cart�o";
    }
}
