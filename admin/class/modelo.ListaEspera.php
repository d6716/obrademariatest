<?php 
class ListaEspera extends Persistencia {
    var $nome;
    var $email;
    var $telefone;
    var $grupo = null;

    public function recuperaTotal($grupo,$busca = ""){
        
            
         $sql = "select count(p.id) as total from ag_lista_espera p  where p.id_grupo = $grupo";
        if($busca != "")
        $sql .= " and (p.nome like '%$busca%')";
        
        $rs = $this->DAO_ExecutarQuery($sql);
		return $this->DAO_Result($rs,"total",0);
	}

	public function pesquisa($inicio,$fim,$grupo,$busca){
    $sql = "select p.* from ag_lista_espera p where p.id_grupo = $grupo";
    if($busca != "")
    $sql .= " and (p.nome like '%$busca%')";

    $sql .= " order by p.id limit $inicio, $fim";
	return $this->getSQL($sql);
    }
    

    public function excluir($id){
        if($this->getById($id)){
            $this->delete($id);
            $_SESSION['tupi.mensagem'] = 'success;Cliente exclu�do da lista com sucesso!';
        }else{
            $_SESSION['tupi.mensagem'] = 'warning;Cliente n�o encontrado';
        }
    }

    public function incluir($request){
        $idGrupo = $this->md5_decrypt($request['idGrupo']);
        $oGrupo = new Grupo();
        $oGrupo->getById($idGrupo);
        //$teste = $this->getRows(0,1,array(),array('grupo'=>'='.$idGrupo,'email'=>"='".$request['email']."'"));
    //if(count($teste) == 0){
        $this->nome = $request['nome'];
        $this->email = $request['email'];
        $this->telefone = $this->limpaDigitos($request['telefone']);
        $this->grupo = $oGrupo;
        $this->save();


        $html = "<p>Inclus�o de cliente em lista de espera de grupo<p>";
        $html .= "<p>Grupo: <br/>".$oGrupo->nomePacote."<p>";
        $html .= "<p>Cliente <br/>Nome:".$this->nome."<br/>Email:".$this->email."<br/>Telefone:".$this->formataTelefone($this->telefone)."<p>";
        $tplemail = new Template($this->URI."templates/tpl_email_ecommerce.html");
        $tplemail->CONTEUDO = $html;
       
        $this->mail_html($this->DESTINATARIO,$this->REMETENTE, 'Obra de Maria DF -  Inclus�o de cliente '.$this->nome, $tplemail->showString());
    
        $html = "<p>Ol�, ".$this->nome.".  Voc� acaba de se inscrever na Lista de espera do grupo de peregrina��es : ".$oGrupo->nomePacote.", Embarque: ".$this->convdata($oGrupo->dataEmbarque,"mtn");
        $orot = new Roteiro();
        $rsrot = $orot->getByGrupo($idGrupo);
        if(count($rsrot)){
            $roteiro = $rsrot[0];
            if($roteiro->padreName != ""){
                $html .= " com ".$roteiro->padreName;
            }
        }
        $html .= ". Na hip�tese de surgir vaga, a obra de maria peregrina��es Bras�lia manter� contato com vc. Atenciosamente. Obra de Maria Df<p>";
        $html .= "<p>Grupo: <br/>".$oGrupo->nomePacote."<p>";
        $html .= "<p>Cliente <br/>Nome:".$this->nome."<br/>Email:".$this->email."<br/>Telefone:".$this->formataTelefone($this->telefone)."<p>";
        $tplemail = new Template($this->URI."templates/tpl_email_ecommerce.html");
        $tplemail->CONTEUDO = $html;
        $this->mail_html($this->email,$this->REMETENTE, 'Obra de Maria DF -  Confirma��o de cadastro', $tplemail->showString());

        $_SESSION['tupi.mensagem'] = 'success;Cliente inclu�do na lista com sucesso!';
        return true;
    //}else{
    //    $_SESSION['tupi.mensagem'] = 'warning;Email de Cliente j� cadastrado na lista do grupo';
     //   return false;
    //}
    }


    public function sendcheckout($id){
        
        $this->getById($id);
        $oGrupo = new Grupo();
        $oGrupo->getById($this->grupo->id);


        $html = "<p>Ol�, ".$this->nome.", como voc� se cadastrou na Lista de espera do grupo de peregrina��es : ".$oGrupo->nomePacote.", Embarque: ".$this->convdata($oGrupo->dataEmbarque,"mtn");
        $html .= ", voc� est� recebendo este email porque surgiu uma vaga no grupo.<br/>";
        $html .= "Para se cadastrar basta entrar no link abaixo exclusivo para seu cadastro. na lista de espera</p>";
        $html .= "<p>Grupo: <br/>".$oGrupo->nomePacote."</p>";
        $html .= "<p>Cliente <br/>Nome:".$this->nome."<br/>Email:".$this->email."<br/>Telefone:".$this->formataTelefone($this->telefone)."</p>";
        $html .= "<a href='".$oGrupo->urlSite."checkoutlist.php?idLista=".$this->md5_encrypt($id)."'>Cadastre-se aqui!</a>";
        $tplemail = new Template($this->URI."templates/tpl_email_ecommerce.html");
        $tplemail->CONTEUDO = $html;
        $this->mail_html($this->email,$this->REMETENTE, 'Obra de Maria DF -  Lista de Espera (Vaga)', $tplemail->showString());

        $_SESSION['tupi.mensagem'] = 'success;Email enviado com sucesso!';
        return true;
    //}else{
    //    $_SESSION['tupi.mensagem'] = 'warning;Email de Cliente j� cadastrado na lista do grupo';
     //   return false;
    //}
    }
}