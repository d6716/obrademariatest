<?php
class ControleBoleto extends Persistencia {
    var $diaVencimento;
    var $dataCriacao;
    var $numeroParcelas;
    var $participante;
    var $grupo;
    var $valorTotal;



    public function incluir($request){
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();
        try{
        $obj = new ControleBoleto();
        $participante = new Participante();
        $dataVencimento = $this->convdata($request['dataVencimento'],"ntm");
        $participante->getById($request['idParticipante']);
        $obj->dataCriacao = date("Y-m-d H:i:s");
        $obj->diaVencimento = substr($request['dataVencimento'],0,2);
        $obj->numeroParcelas = $request['parcelas'];
        $obj->participante = $participante;
        $obj->grupo = $participante->grupo;
        $obj->valorTotal = $this->money($request['valorTotal'],"bta");
        $obj->save();

            //gerar os boletos
        $date1 = DateTime::createFromFormat("Y-m-d",$dataVencimento);
        $valorParcela = $this->money($obj->valorTotal / $obj->numeroParcelas,"bta");
        for ($i=0; $i < $obj->numeroParcelas ; $i++){
            $oParcela  = new ControleBoletoParcela();
            $oParcela->valorMoeda = $valorParcela;
            $oParcela->valorReal = null;
            $oParcela->cotacao = null;
            $oParcela->controleBoleto = $obj;
            $oParcela->isGerado = 0;
            $oParcela->numero = $i+1;
            $oParcela->dataVencimento = $date1->format("Y-m-d");           
            $oParcela->save();
            $date1->add(new DateInterval('P1M'));
        }

        
        return $this->conn->commit();
        }catch(Exception $e){
            $this->conn->rollback();
            $_SESSION['tupi.mensagem'] = 'danger;'.$e->getMessage();
            return false;
        }
        
    }

    public function excluir($request){
        $this->conn->autocommit(false);
        $this->conn->begin_transaction();
        try{
        $obj = new ControleBoleto();
        if($obj->getRow(array("participante"=>"=".$request['idParticipante']))){
            $this->delete($obj->id);
        }
        return $this->conn->commit();
        }catch(Exception $e){
            $this->conn->rollback();
            $_SESSION['tupi.mensagem'] = 'danger;'.$e->getMessage();
            return false;
        }
        
    }
}