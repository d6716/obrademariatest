<?php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

include("../tupi.inicializar.php");
try{

    // Create some handlers
    $stream = new StreamHandler(__DIR__.'/logs/'.date("Ymd").'log_cielo.log', Logger::DEBUG);
    $logger = new Logger('cielo');
    $logger->pushHandler($stream);
    $obj = new MyCieloCheckout();
    $url  = isset($_REQUEST['URL']) ? $_REQUEST['URL'] : '';
    $idVenda = isset($_REQUEST['MerchantOrderNumber']) ?$_REQUEST['MerchantOrderNumber'] : '0';
    $return = $obj->UpdateByNotification($url,$idVenda,$logger);
    $obj->mail_html($obj->DESTINATARIO,$obj->REMETENTE, 'Obra de Maria DF - Cópia email confirmação de pagamento', 'Atualização da venda: '.$idVenda.' Pagamento Cielo');
    @$logger->info('sucesso-'.date("H:i:s")."-".$return);    
    echo json_encode(array("code"=>"200","data"=>array("message"=>utf8_encode($return))));
}catch (Exception $e){    
    $mensagem = utf8_encode($e->getMessage());
    @$logger->error('erro-'.date("H:i:s")."-".$e->getMessage());
    $obj->mail_html($obj->DESTINATARIO,$obj->REMETENTE, 'Obra de Maria DF - ERRO DE INTEGRAÇÃO', 'OCORREU UM ERRO NA NOTIFICAÇÃO DA CIELO: '.$e->getMessage());
    echo json_encode(array("code"=>"500","data"=>array("mensagem"=>"$mensagem")));
}