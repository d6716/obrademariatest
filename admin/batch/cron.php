<?php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
include("../tupi.inicializar.php");
try{
	$stream = new StreamHandler(__DIR__.'/logs/'.date("Ymd").'log_agendamento.log', Logger::DEBUG);
    $logger = new Logger('agendamento');
    $logger->pushHandler($stream);
    //executa agendamentos
	$ag = new Agendamento();
	if(!$ag->debug){
			$ag->enviarEmailsAniversariantes();
			@$logger->info('sucesso-'.date("H:i:s")."-Aniversariantes"); 
			$ag->enviarEmailsCartoesPrePagos();
			@$logger->info('sucesso-'.date("H:i:s")."-Cartoes Pr�pagos"); 
			$ag->enviarEmailsContasAPagar();
			@$logger->info('sucesso-'.date("H:i:s")."-Contas a Pagar"); 
			$ag->enviarEmailsPassaportes();
			@$logger->info('sucesso-'.date("H:i:s")."-Passaportes"); 
			$ag->enviarEmailsChegadaGrupo();
			@$logger->info('sucesso-'.date("H:i:s")."-Chegada de Grupo"); 
			$ag->enviarEmailBoletosAVencer();	
			@$logger->info('sucesso-'.date("H:i:s")."-Boletos a Vencer"); 
	}	
	$ag->atualizaDollar();
	@$logger->info('sucesso-'.date("H:i:s")."-Atualiza Dollar"); 
}catch (Exception $e){	
	@$logger->error('erro-'.date("H:i:s")."-".$e->getMessage());
   
}