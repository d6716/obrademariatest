<?php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

include("../tupi.inicializar.php");
try{
    $stream = new StreamHandler(__DIR__.'/logs/'.date("Ymd").'log_gerencianet.log', Logger::DEBUG);
    $logger = new Logger('site');
    $logger->pushHandler($stream);
    $obj = new GerenciaNetCheckOut();
    $return = $obj->UpdateByNotification();
    @$logger->info('sucesso-'.date("H:i:s")."-".$return);
    echo json_encode(array("code"=>"200","data"=>array("message"=>utf8_encode($return))));
}catch (Exception $e){
    $mensagem = utf8_encode($e->getMessage());
    @$logger->error('erro-'.date("H:i:s")."-".$e->getMessage());
    $obj->mail_html($obj->DESTINATARIO,$obj->REMETENTE, 'Obra de Maria DF - ERRO DE INTEGRAÇÃO', 'OCORREU UM ERRO NA NOTIFICAÇÃO DA GERENCIA NET: '.$e->getMessage());
    echo json_encode(array("code"=>"500","data"=>array("mensagem"=>"$mensagem")));
}