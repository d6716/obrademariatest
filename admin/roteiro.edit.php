<?php
include("tupi.inicializar.php");
include("tupi.template.inicializar.php");
$codAcesso = 51;

include("tupi.seguranca.php");

$obRoteiro = new Roteiro();
$obGrupo = new Grupo();
$obFoto = new Foto();
$obVideo = new Video();
$obEtinerario = new Etinerario();

$id = isset($_REQUEST['id']) ? $obRoteiro->md5_decrypt($_REQUEST['id']) : 0;
$aba = isset($_REQUEST['aba']) ? $_REQUEST['aba'] : 0;
$tpl->RADIONAO = 'checked="true"';
$tpl->PUBNAO = 'checked="true"';
$tpl->PRONAO = 'checked="true"';
$tpl->LENAO = 'checked="true"';
$tpl->ACITVE_0 = $aba == 0 ? 'active' : '';
$tpl->ACITVE_1 = $aba == 1 ? 'active' : '';
$tpl->ACITVE_2 = $aba == 2 ? 'active' : '';
$tpl->ACITVE_3 = $aba == 3 ? 'active' : '';
$tpl->ACITVE_4 = $aba == 4 ? 'active' : '';
$tpl->ACITVE_5 = $aba == 5 ? 'active' : '';



if($id != 0){
if(!$obRoteiro->getById($id)){
    //$obRoteiro->getById(1);
    echo "roteiro nao encontrado";
    exit();
}
$tpl->LABEL_ROTEIRO = $obRoteiro->title;
$tpl->ID = $obRoteiro->id;
$tpl->DURACAO = $obRoteiro->grupo->duracao;
$tpl->MAX_PESSOA = $obRoteiro->grupo->maxPessoa;
$tpl->IDADE_MINIMA = $obRoteiro->grupo->idadeMinima;
$tpl->LOCAL = $obRoteiro->grupo->local;
$tpl->PESQUISA = $obRoteiro->pesquisa;
$tpl->UNLIKES = $obRoteiro->unlikes;
$tpl->LIKES = $obRoteiro->likes;
$tpl->RADIOSIM =$obRoteiro->countDown == 1 ? 'checked="true"' : '';
$tpl->RADIONAO =$obRoteiro->countDown == 0 ? 'checked="true"' : '';
$tpl->PUBSIM =$obRoteiro->publish == 1 ? 'checked="true"' : '';
$tpl->PUBNAO =$obRoteiro->publish == 0 ? 'checked="true"' : '';
$tpl->CHECKED_OCE = strpos($obRoteiro->continent,"OCEANIA") !== false ? 'checked="true"' : '';
$tpl->CHECKED_EUR = strpos($obRoteiro->continent,"EUROPA") !== false ? 'checked="true"' : '';
$tpl->CHECKED_ASIA = strpos($obRoteiro->continent,"�SIA") !== false ? 'checked="true"' : '';
$tpl->CHECKED_AMC = strpos($obRoteiro->continent,"AM�RICA CENTRAL") !== false ? 'checked="true"' : '';
$tpl->CHECKED_AMS = strpos($obRoteiro->continent,"AM�RICA DO SUL") !== false ? 'checked="true"' : '';
$tpl->CHECKED_AMN = strpos($obRoteiro->continent,"AM�RICA DO NORTE") !== false ? 'checked="true"' : '';
$tpl->CHECKED_AFRICA = strpos($obRoteiro->continent,"�FRICA") !== false ? 'checked="true"' : '';
$tpl->CARD_IMAGE = $obRoteiro->cardImage != '' ? $obRoteiro->cardImage : 'placeholder.jpg';
$tpl->CARD_DESCRIPTION = $obRoteiro->cardDescription;
$tpl->CARD_VALUE = $obRoteiro->cardValue;
$tpl->CARD_TITLE = $obRoteiro->cardTitle;
$tpl->IMAGE = $obRoteiro->image != '' ? $obRoteiro->image  : 'placeholder.jpg';
$tpl->DESCRIPTION = $obRoteiro->description;
$tpl->TITLE = $obRoteiro->title;
$tpl->PADRE_NAME = $obRoteiro->padreName;
$tpl->PADRE_IMAGE = $obRoteiro->padreImage != '' ?  $obRoteiro->padreImage : 'placeholder.jpg';
$tpl->TEXTO_PROMOCAO = $obRoteiro->textoPromocao;
$tpl->DESCONTO_PROMOCAO = $obRoteiro->descontoPromocao;
$tpl->PROSIM =$obRoteiro->bitPromocao == 1 ? 'checked="true"' : '';
$tpl->PRONAO =$obRoteiro->bitPromocao == 0 ? 'checked="true"' : '';

$tpl->LESIM =$obRoteiro->bitListaEspera == 1 ? 'checked="true"' : '';
$tpl->LENAO =$obRoteiro->bitListaEspera == 0 ? 'checked="true"' : '';

$rsfotos = $obFoto->getByRoteiro($obRoteiro->id);
foreach($rsfotos as $key => $value){
    $tpl->FOTO = $value->nameThumb;
    $tpl->ID_FOTO = $value->id;
    $tpl->block("BLOCK_FOTO");
}

foreach($obRoteiro->reviews as $key => $value){
    $tpl->FOTO_REVIEW = $value->photo;
    $tpl->ID_REVIEW = $value->id;
    $tpl->COMENT_REVIEW = $value->review;
    $tpl->COMENT_NAME = $value->name;
    $tpl->COMENT_DATE = $obRoteiro->convdata($value->date,"mtnh");
    $tpl->block("BLOCK_REVIEW");
}





foreach($obRoteiro->itineraryes as $key => $value){
    $tpl->IT_ORDER = $value->order;
    $tpl->IT_TITLE = $value->title;
    $tpl->IT_DESCRIPTION = $value->description;
    $tpl->IT_ID = $value->id;
    $tpl->block("BLOCK_ETINERARIO");
}

$rsVideos = $obVideo->getByRoteiro($obRoteiro->id);
if(count($rsVideos)>0){
$tpl->VIDEO = $rsVideos[0]->name;
}
$tpl->block("BLOCK_EDITAR");
$tpl->block("BLOCK_EDITAR_PILL");
//$rsGrupos = $obGrupo->getRows(0,999,array("id"=>"desc"),array("status"=>"=1"));
}
$rsGrupos = $obGrupo->getGrupoSemRoteiro();
if($id != 0){
    array_push($rsGrupos,$obRoteiro->grupo);
}

foreach ($rsGrupos as $key => $value) {
    $tpl->ID_GRUPO = $value->id;
    $tpl->NOME_GRUPO = $value->id."-".$value->nomePacote;
    if($id != 0)
        $tpl->SELECTED_GRUPO = $value->id == $obRoteiro->grupo->id ? 'selected' : '';
    $tpl->block('BLOCK_GRUPO');
}



include("tupi.template.finalizar.php"); 